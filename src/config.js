const config = {
    routes: {
        home:'/',
        following: '/following',
        postDetail: '/@:nickname/video/:videoId',
        profile: '/@:nickname',
        search: '/search',
        upLoad: '/upload',
        message: '/message'
    },
    socials: {
        shares: {
            whatsapp: url => `https://api.whatsapp.com/send/?text=${encodeURIComponent(url)}`,
            facebook: url => `https://www.facebook.com/sharer/sharer.php?u=${encodeURIComponent(url)}`,
            twitter: url => `https://twitter.com/intent/tweet?refer_source=${encodeURIComponent(url)}`,
            reddit: url => `https://www.reddit.com/submit?url=${encodeURIComponent(url)}`,
            printerest: url => `http://pinterest.com/pin/create/button/?url=${encodeURIComponent(url)}`
        }
    }

}

export default config