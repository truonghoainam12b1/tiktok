import { useContext } from 'react' 
import Modal from '~/packages/namth-modal'
import Login from '~/containers/Login'
import Register from '~/containers/Register'
import { LOGIN_MODAL , REGISTER_MODAL } from './constants'
import ModalContex from './ModalContext'
function ModalProvider () {
   const context = useContext(ModalContex)
   console.log(context)
   const handleLoginSuccess = () => {
      context.setModal(null)
      window.location.reload()
   }
   return(
      <>
         <Modal
               isOpen={context.current === LOGIN_MODAL}
               noPadding
               onClickClose = {() => context.setModal(null)}
         >
            <Login
               onSuccess={handleLoginSuccess}
               onSwitchRegister={() => context.setModal(REGISTER_MODAL)}
            />
         </Modal>

         <Modal
            isOpen={context.current === REGISTER_MODAL}
            noPadding
            onClickClose={() => context.setModal(null)}
         >
            <Register
               onSwitchLogin={() => context.setModal(LOGIN_MODAL)}
            />
         </Modal>
      </>
   )
}

export default ModalProvider