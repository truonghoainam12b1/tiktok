
import styles from './TextInput.module.scss'

function TextInput ({
   label = '',
   value = '',
   onChange = () => {},
   message = '',
   ...inputProps
}) {
   return (
      <div className={styles.wrapper}>
         {!!label && (
            <p className={styles.label} >{label}</p>
         )}
         <div className={styles.inputBlock}>
            <input
               className={styles.input}
               value={value}
               onChange={onChange}
               {...inputProps}
            />
         </div>
         {!!message && (
            <p className={styles.message}>{message}</p>
         )}
      </div>
   )
}

export default TextInput
