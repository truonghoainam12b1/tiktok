import styles from './Button.module.scss'
import { Link } from 'react-router-dom'


function Button({
    to = '',
    href = '' ,
    size = "m",
    type ="primary",
    children = null,
    underline = false,
    disabled = false,
    openNewTab = false,
    onClick = () => {},
    ...restProps
}) {
    const props = {}
    let Component = 'button'
    if (href) {
        Component = 'a'
        props.href = href
        if (openNewTab) {
            props.target = '_blank'
        }
    }
    if (to) {
        Component = Link
        props.to = to
    }
    const className= [styles.wrapper,
                    styles[type],
                    styles[size],
                    underline ? styles.underline : '',
                    disabled ? styles.disabled : ''
                    ]
    return (
        <Component 
            {...props}
            {...restProps}
            className={className.join(' ')}
            onClick={onClick}>
            <span>{children}</span>
        </Component>
    )
}

export default Button
