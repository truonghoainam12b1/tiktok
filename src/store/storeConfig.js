import { createStore } from 'redux'

import reduce from './reducer'

function storeConfig() {
   const store = createStore(reduce)
   return store

}
export default storeConfig