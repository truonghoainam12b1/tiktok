import { SET_CURRENT_USER } from './const'

export function setCurrentUser(user) {
   return {
      type: SET_CURRENT_USER,
      payload: user,
   }
}