import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import 'overlayscrollbars/css/OverlayScrollbars.css'
import { Provider } from 'react-redux'

import App from './components/App';
import reportWebVitals from './reportWebVitals';
import './assets/styles/global.scss'
import store from '~/store'

//Global config axios
axios.defaults.baseURL = 'https://tiktok.f8team.dev'
axios.defaults.headers.common['Authorization'] = 'Bearer ' + window.localStorage.getItem('token')
axios.interceptors.response.use(function (response) {
  return response.data
}, function (error) {
  return Promise.reject(error)
});

 
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
