import { useState } from 'react'
import axios from 'axios'

import Button from '~/packages/namth-button'
import {Wrapper ,
         MethodItem,
         Footer ,} from '~/components/Auth'
import TextInput from '~/packages/namth-textInput'


import userImg from '~/assets/img/userImg.png'
import facebookImg from '~/assets/img/facebookImg.png'
import googleImg from '~/assets/img/GoogleImg.png'
import intagramImg from '~/assets/img/intagram.png'
import katalkImg from '~/assets/img/kakaotalk.svg'
import twitterImg from '~/assets/img/twitter.png'

const FORM_EMAIL_PASSWORD = 'FORM_EMAIL_PASSWORD'


function Register({
   onSwitchLogin = () => {},
   onRegisterSuccess = () => {}
}) {
   const [ email , setEmail ] = useState('')
   const [ password , setPassword ] = useState('')
   const [ forms , setForms ] = useState([])
   const [ errors , setErrors ] = useState({})

   const pushForm = (form) => {
      setForms([...forms , form])
   }

   const getCurrentForm = () => {
      if (!forms.length) return 
      return forms[forms.length - 1]
   }


   const getFormTitle = () => {
      const currentForm = getCurrentForm()
      switch (currentForm) {
         case FORM_EMAIL_PASSWORD : 
            return 'Log up'
         default: 
            return 'Sign up for TikTok'
      }
   }
   const handleLogUp = () => {
      axios.post('/api/auth/register' , { 
         type : 'email',
         email ,
         password})
         .then(res => {
               console.log(res)
               window.localStorage.setItem('token', res.meta.token)
               onRegisterSuccess()}
            )
         .catch(err => {
            switch(err.response.status) {
               case 422:
                  const resErrors = {}
                  Object.keys(err.response.data.errors).forEach(field => {
                        resErrors[field] = err.response.data.errors[field][0]
                  })
                  setErrors(resErrors)
                  break;
               default:
                  setErrors({
                     ...errors,
                     password:'An error has occurred, please contact admin@gmail.com'
                  })
            }
         })
   }

   const handleBackForm = () => {
      const formClone = forms.slice(0)
      formClone.pop()
      setForms(formClone)
   }
   const currentForm = getCurrentForm()
   return(
      <Wrapper
         heading={getFormTitle()}
         showBackBtn={!!currentForm}
         onBack={handleBackForm}
         renderFooter={() => 
            <Footer
               text = 'Already have an account?'
               actionTitle = 'Sign in'
               onAction = {onSwitchLogin}
            />
         }
      >
      {currentForm === FORM_EMAIL_PASSWORD ? (
         <>
            <TextInput
                     label = 'Email'
                     type='text'
                     placeholder="Email or username"
                     value={email}
                     outoFocus
                     message = {errors.email}
                     onChange = {(e) => {
                        setEmail(e.target.value)
                        setErrors({
                           ...errors,
                           email: null
                        })
                     }}
                  />
                  <TextInput
                     label = 'Password'
                     type = 'password'
                     placeholder="Password"
                     value={password}
                     message = {errors.password}
                     onChange = {(e) => {
                        setPassword(e.target.value)
                        setErrors({
                           ...errors,
                           password: null
                        })
                     }}
                  />
                  <Button
                     size='l'
                     style={styles.loginBtn}
                     disabled={!email || !password}
                     onClick={handleLogUp}
                  >
                     Log up
                  </Button>
         </>
      ) : (
         <>
            <MethodItem
               title = 'Use phone or email'
               image = {userImg}
               onClick = {() => {
                  pushForm(FORM_EMAIL_PASSWORD)
               }}
            />
            <MethodItem
               title = 'Continue with Facebook'
               image = {facebookImg}
               onClick = {() => {}}
            />
            <MethodItem
               title = 'Continue with Google'
               image = {googleImg}
               onClick = {() => {}}
            />
            <MethodItem
               title = 'Continue with Twitter'
               image = {twitterImg}
               onClick = {() => {}}
            />
            <MethodItem
               title = 'Continue with Kakaotalk'
               image = {katalkImg}
               onClick = {() => {}}
            />
            <MethodItem
               title = 'Continue with Instagram'
               image = {intagramImg}
               onClick = {() => {}}
            />
         </>
      )}
      </Wrapper>
   )
}
const styles = {
   loginBtn: {
   width: '100%',
   marginTop: 32,
   fontWeight: 'bold' 
}
}

export default Register