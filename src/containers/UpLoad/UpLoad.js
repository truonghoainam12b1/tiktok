import { UpLoadComponent ,
         UpLoadContent,
         FooterUpLoad,
} from '~/components/UpLoad'

function UpLoad () {
   return(
      <UpLoadComponent>
         <UpLoadContent/>
         <FooterUpLoad/>
      </UpLoadComponent>
   )
}

export default UpLoad