

import { useEffect , useState } from 'react'
import axios from 'axios'

import Follow from '~/components/Follow'
import FollowingEntity from '~/entities/Following'
import UserContext from '~/contexts/UserContext'
import FollowingVideo from '~/containers/FollowingVideo'


function Following () {
    const [ follow , setFollow ] = useState([])
    const [ currentAccount , setCurrentAccount ] = useState(null)
    const [pagination , setPagination] = useState({
        currentPage : 1,
        totalPages : 0,
        total : 0,
        perPage : 0,
    })
    // useEffect(() => {
    //     axios.get('/api/me/followings?page=1')
    //         .then( res => {
    //             setFollowing(FollowingEntity.createFromList(res.data))
    //         })
    // },[])
    useEffect(() => {
        axios.get(`/api/users/suggested?page=${pagination.currentPage}&per_page=12`)
        .then( res => {
            const accounts = FollowingEntity.createFromList([...follow , ...res.data])
            setFollow(accounts)
            if(accounts.length > 0) {
                setCurrentAccount(accounts[0])
            }
            setPagination({
                ...pagination,
                totalPages: res.meta.pagination.total_pages,
                total: res.meta.pagination.total,
                perPage: res.meta.pagination.per_page,
            })
        })
        .catch( err => {
            console.log(err)
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[pagination.currentPage])
    const handleLoadMore = () => {
            if (pagination.currentPage >= pagination.totalPages) return
            setPagination({
                currentPage: pagination.currentPage + 1,
            });
    }
    const checkPlaying = account => {
        return !!currentAccount && currentAccount.id === account.id
    }
    const handleMouseEnter = (account) => {
        setCurrentAccount(account)
    }
    const handleOnFollow = () => {
        console.log('nam')
    }
    

    return (
        <UserContext.Consumer>
            {currentUser => (
                !!currentUser ? (
                    <FollowingVideo/>
                ) : (
                    <Follow 
                        data = {follow}
                        pagination = {pagination.currentPage}
                        onLoadMore={handleLoadMore}
                        onMouseEnter={handleMouseEnter}
                        isPlaying={checkPlaying}
                        onClickFollow={handleOnFollow}
                    />
                )
            )}
        </UserContext.Consumer>
    )
}

export default Following