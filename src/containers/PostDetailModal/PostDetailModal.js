
import { useState , useEffect , useRef , useContext } from 'react'
import { useHistory } from 'react-router-dom'
import axios from 'axios'

import { Wrapper ,
        Content ,
        PostInfo ,
        Comment ,
        CommentItem ,
        VideoPlayer } from '~/components/PostDetailModal'
import PostEntity from '~/entities/Post'
import { ModalContext , LOGIN_MODAL } from '~/modules/ModalProvider'

function PostDetail({
    currentTime,
    onRequestClose,
    data,
    videoId,
    isMuted,
    showNext,
    isPlaying,
    showPrev,
    onPrevVideo = () => {},
    onNextVideo = () => {},
    onToggleMute = () => {},
    onToggleLike = () => {},
    onTogglePlaying = () => {},
    onClickShare = () => {},
}) {
    const videoRef = useRef(null)
    const [ comments , setComments ] = useState([])
    const [ commentText , setCommentText ] = useState('')
    const [ pagination , setPagination] = useState({
        currentPage : 1,
        totalPages : 0,
        total : 0,
        perPage : 0,
    })
    const history = useHistory()
    const [ postDetail , setPostDetail ] = useState(null)
    const [isEmojiBox, setIsEmojiBox] = useState(false)
    const [chosenEmoji, setChosenEmoji] = useState(null)
    const model = useContext(ModalContext)
    useEffect(() => {
        axios.get(`/api/posts/${videoId}`)
            .then(res => {
                setPostDetail(PostEntity.create(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    },[videoId])
    const handleVideoRef = (ref) => {
        if(ref) {
            videoRef.current = ref
            const newVideoRefTime = videoRef.current.currentTime
            if(newVideoRefTime) return

            videoRef.current.play()
            videoRef.current.currentTime = currentTime
        }
    }
    
    const postId = useRef(videoId)
    
    useEffect(() => {
        const newPostId = videoId
       
        if(postId.current !== newPostId) {
            setComments([])
            setPagination({
                currentPage : 1,
            })
            postId.current = newPostId
        }
        axios.get(`/api/posts/${videoId}/comments?page=${pagination.currentPage}`)
            .then(res => {
                if(pagination.currentPage === 1) {
                    setComments(PostEntity.createFromList(res.data))
                    setPagination({
                        ...pagination,
                        totalPages: res.meta.pagination.total_pages,
                        total: res.meta.pagination.total,
                        perPage: res.meta.pagination.per_page,
                    })
                }
                else {
                    setComments(prevState => [...prevState , ...PostEntity.createFromList(res.data)])
                }
            })
            .catch (err => {
                console.log(err)
            })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[videoId , pagination.currentPage])
    const headleChangeText = (e) => {
        setCommentText(e.target.value)
    }
    const headleOnClickPost = () => {
            if(!commentText) return

            axios.post(`/api/posts/${videoId}/comments`, { comment: commentText })
                .then(res => {
                    setComments(prevState => PostEntity.createFromList([res.data , ...prevState]))
                    setCommentText('')
                })
                .catch(err => {
                    console.log(err)
                })
    }
    const headleOnClickLogin = () => {
        model.setModal(LOGIN_MODAL)
    }
    const onLoadMoreComments = () => {
        if (
            pagination.currentPage < pagination.totalPages
            ) {
                setPagination({
                    ...pagination,
                    currentPage: pagination.currentPage + 1
                })
            }
    }
    const headleOnClickCopy = (link) => {
        navigator.clipboard.writeText(link)
    }
    const handleShowProfile = (nickname) => {
        history.push(`/@${nickname}`)
    }
    const headleEmojiClick = (emojiObject) => {
        setChosenEmoji(prev => [...prev, emojiObject])
    }

    const headleToggleEmojiIcon = () => {
        setIsEmojiBox(!isEmojiBox)
    }
    if (!postDetail) {
        return null
    }
    return (
        <Wrapper onRequestClose={() => {onRequestClose(videoRef.current.currentTime)}}>
            <VideoPlayer
                postDetail={data}
                getVideoRef={handleVideoRef}
                onPrevVideo={onPrevVideo}
                onNextVideo={onNextVideo}
                onNext={showNext}
                onPrev={showPrev}
                isMuted={isMuted}
                isPlaying={isPlaying}
                onToggleMute={onToggleMute}
                onTogglePlaying={onTogglePlaying}
                onClickPlay={onTogglePlaying}
            />
            <Content>
                <PostInfo 
                    key={data.id}
                    postDetail={data}
                    onToggleLike={onToggleLike}
                    onClickCopy={headleOnClickCopy}
                    showProfile={handleShowProfile}
                    onClickShare={onClickShare}
                />
                <Comment
                    onClickPost={headleOnClickPost}
                    comment={commentText}
                    onChangeText={headleChangeText}
                    onClickLogin={headleOnClickLogin}
                    isChosenEmoji={isEmojiBox}
                    // comment={chosenEmoji}
                    onEmojiClick={headleEmojiClick}
                    onToggleEmojiIcon={headleToggleEmojiIcon}
                >
                    {comments.map((comment, index) => 
                        <CommentItem
                            postAuthorId={postDetail?.user.id}
                            key={comment.id}
                            data={comment}
                            isLast={comments.length - 1 === index}
                            onEnter={onLoadMoreComments}
                            showProfile={handleShowProfile}
                        /> 
                    )}
                </Comment>
            </Content>
        </Wrapper>
    )
}

export default PostDetail