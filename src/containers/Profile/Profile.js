import { useEffect , useState , useRef} from 'react'
import { useParams , useHistory } from 'react-router-dom'
import axios from 'axios'

import ProfileComponent from '~/components/Profile/Profile'
import {
        ProfileInfo 
        }from '~/components/Profile'
import Account from '~/entities/Account'
import PostDetailModal from '~/containers/PostDetailModal'
import storage from '~/utils/storage'
import Post from '~/entities/Post'
import config from '~/config'


const NEXT_POST = 'NEXT_POST'
const PREV_POST = 'PREV_POST'

function Profile() {
    const { nickname } = useParams()
    const [ isMuted , setIsMuted ] = useState(storage.get('isMuted', true))
    const [ isPlaying , setIsPlaying] = useState(true)
    const [ currentPost , setCurrentPost ] = useState(null)
    const [ videoCurrent , setCurrentVideo ] = useState(null)
    const [ profile , setProfile] = useState(null)
    const videoRefs = useRef({})
    const posts = profile?.posts

    const history = useHistory()

    useEffect(() => {
        axios.get('/api/users/@' + nickname)
        .then( res => {
            const User = Account.create(res.data)
            setProfile(User)
            if(User?.posts.length > 0) {
                setCurrentVideo(User?.posts[0])
            }
        })
        .catch( err => {
            console.log(err)
        })
        .finally( () => {
            const videos = profile?.posts
            if(videos?.length > 0) {
                setCurrentVideo(videos[0])
            }
        })
    } , ['/api/users/' + nickname])
    
    const checkPlaying = video => {
        return !!videoCurrent && videoCurrent.id === video.id
    }
    const handleMouseEnter = video => {
        setCurrentVideo(video)
    }
    
    const handleShowDetail = (data) => {
        // console.log(data)
        setCurrentPost(Post.create(data))
        const postUrl = `/@${data.user.nickname}/video/${data.uuid}`
        window.history.pushState(null , document.title , postUrl) 
    }
    const getCurrentIndex = () => {
        return posts.findIndex(post => post.id === currentPost.id)
    }
    const handleToggleMute = (post) => {
            setIsMuted(!isMuted)
            storage.set('isMuted', !isMuted)
    }
    const handleCloseDetailPost = () => {
        setCurrentPost(null)
        window.history.back()
    }
    const createChangePost = (type) => {
        return () => {
            const currentIndex = getCurrentIndex()
            let newPost
            if ( type === NEXT_POST ) {
                newPost = posts[currentIndex + 1]
            }
            if ( type === PREV_POST ) {
                newPost = posts[currentIndex - 1]
            }
            setCurrentPost(newPost)
        }
    }
    const handleToggleModalPlaying = () => {
        setIsPlaying(!isPlaying)
    }
    const handleClickMessage = () => {
        history.push(`${config.routes.message}`)
    }
    const handleToggleLike = () => {
      
    }
    const handleToggleFollow = () => {}

    if(!profile) {
        return <h2>Loading...</h2>
    }
    return (
        <>
            <ProfileComponent 
                profile={profile}
                isPlaying={checkPlaying}
                onMouseEnter={handleMouseEnter}
                onShowDetail={handleShowDetail}
            >
                <ProfileInfo
                    key = {profile.id}
                    data = {profile}
                    onClickMessage = {handleClickMessage}
                    onToggleFollow = {handleToggleFollow}
                />
            </ProfileComponent>
            {currentPost && (
                <PostDetailModal
                    data = {currentPost} 
                    videoId = {currentPost?.id}
                    isPlaying={isPlaying}
                    onTogglePlaying = {handleToggleModalPlaying}
                    currentTime = {0}
                    onToggleMute = {handleToggleMute}
                    isMuted={isMuted}
                    onRequestClose={handleCloseDetailPost} 
                    onPrevVideo = {createChangePost(PREV_POST)}
                    onNextVideo = {createChangePost(NEXT_POST)}
                    showNext = {getCurrentIndex() < posts.length - 1}
                    showPrev = {getCurrentIndex() > 0}
                    onToggleLike = {handleToggleLike}
                    onToggleFollow = {handleToggleFollow}
                />
            )}
        </>
    )
}

export default Profile