import axios from 'axios'
import { useEffect , useState , useRef} from 'react'
import { useLocation , useHistory } from 'react-router-dom'

import AccountSearch from '~/entities/AccountSearch'
import SearchAllName from '~/components/SearchAllName'

function SearchAll () {
        const location = useLocation()
        const history = useHistory()
        let searchValue = location.search
        const [ searchAll , setSearchAll ] = useState([])
        const [pagination , setPagination] = useState({
            currentPage : 1,
        })
        const searchValueRef = useRef(searchValue)
        useEffect(() => {
            const newSearchValue = searchValue
            if( newSearchValue !== searchValueRef.current) {
                setSearchAll([])
                searchValueRef.current = newSearchValue
                setPagination({
                    currentPage : 1
                })
            }
            axios.get(`/api/users/search${searchValue}&type=more&page=${pagination.currentPage}`)
                .then( res => {
                    if (pagination.currentPage === 1) {
                        setSearchAll(AccountSearch.createFromList(res.data))
                    } else {
                        setSearchAll(prevState => [...prevState , AccountSearch.createFromList(res.data)])
                    }
                })
                .catch( err => {
                    console.log(err)
                })
        // eslint-disable-next-line react-hooks/exhaustive-deps
        } , [searchValue , pagination.currentPage])
        
        const handleOnClick = (nickname) => {
            history.push(`/@${nickname}`)
        }
        const handleLoadMore = () => {
            setPagination({
                currentPage: pagination.currentPage + 1,
            });
        }
    return <SearchAllName 
                searchAll = {searchAll}
                onClick={handleOnClick}
                onLoadMore={handleLoadMore}
            />
}

export default SearchAll 
