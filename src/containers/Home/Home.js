
import { useEffect , useState , useRef , useCallback , useContext } from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'
import axios from 'axios'

import { selectors as UserSelectors } from '~/state/user'
import Post from '~/components/Post'
import PostEntity from '~/entities/Post'
import PostDetailModal from '../PostDetailModal'
import storage from '~/utils/storage'
import BrowseHeading from '~/components/PostDetail/BrowseHeading'
// import UserContext from '~/contexts/UserContext'
import { ModalContext, LOGIN_MODAL } from '~/modules/ModalProvider'
import config from '~/config'

const NEXT_POST = 'NEXT_POST'
const PREV_POST = 'PREV_POST'

function Home({
    type = 'for-you'
}) {
    
    const [ posts , setPosts ] = useState([])
    const [ currentPost , setCurrentPost ] = useState(null)
    const [ postInViewport, setPostInViewport ] = useState(null)
    const [ isMuted , setIsMuted ] = useState(storage.get('isMuted', true))
    const videoRefs = useRef({})
    const currentTime = useRef(null)
    const stopWhenPause = useRef(true)
    const [ postDetail , setPostDetail ] = useState(null)
    const { videoId }= useParams()
    const [ isPlaying , setIsPlaying ] = useState(true)
    // const user = useContext(UserContext)
    const modal = useContext(ModalContext)
    const isAuthenticated = useSelector(UserSelectors.isAuthenticated)

    useEffect(() => {
        if(!videoId) return
        axios.get(`/api/posts/${videoId}`)
            .then(res => {
                setPostDetail(PostEntity.create(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    },[videoId])

    useEffect(( ) => {
        window.history.scrollRestoration = 'manual'
        //Get post 
        
        axios.get(`/api/posts?type=${type}&page=1${postDetail ? '&except=' + videoId : ''}`)
            .then(res => {
                setPosts(PostEntity.createFromList(res.data))
            })
            .catch(err => {
                console.log(err)
            })
        return () => {
            window.history.scrollRestoration = 'auto'
        }
    } , [postDetail])
    

    const setVideoRefByPostId = (postId , ref) => {
        return videoRefs.current[postId] = ref
    }

    const handleVideoRef = (ref, post) => {
        setVideoRefByPostId(post.id, ref)
    }

    const handleWaypointEnter = (post) => {
        if(currentPost) return
        stopWhenPause.current = true
        setPostInViewport(post)
    }

    const checkPlaying = (post) => {
        return !!postInViewport && postInViewport.id === post.id
    }

    const handleShowDetail = post => {
        // Pause current video
        const videoRef = videoRefs.current[post?.id]
        if(videoRef) {
            videoRef.pause()
            currentTime.current = videoRef.currentTime
        }
        setCurrentPost(post)
        const postUrl = `/@${post.user.nickname}/video/${post.uuid}`
        window.history.pushState(null , document.title , postUrl)
        
        setIsPlaying(true)
    }

    const handleCloseDetailPost = (currentTime) => {
        const videoRef = videoRefs.current[currentPost.id]
        
        if(videoRef && postInViewport ) {
            videoRef.currentTime = currentTime
            videoRef.play()
        }

        setPostInViewport(currentPost)
        setCurrentPost(null)
        window.history.back()

    }
    // Handle puse/play POSTITEM

    const handleTogglePlay = (post) => {
        stopWhenPause.current = false
        if(checkPlaying(post)) {
            setPostInViewport(null)
        } else {
            setPostInViewport(post)
        }
    }

    const handleToggleModalPlaying = () => {
        setIsPlaying(!isPlaying)
    }

    const handleToggleMute = (post) => {
        const videoRef = getVideoRefById(post.id)
        if(videoRef) {
            videoRef.muted = !videoRef.muted
            setIsMuted(videoRef.muted)
            storage.set('isMuted' , videoRef.muted)
        }
    }

    // Handle the next button and the prev button for postDetailModal
    const getUrl = (post) => {
        return `/@${post.user.nickname}/video/${post.uuid}`
    }

    const getCurrentIndex = () => {
        return posts.findIndex(post => post.id === currentPost.id)
    }

    const createChangePost = (type) => {
        return () => {
            currentTime.current = 0
            const currentIndex = getCurrentIndex()
            let newPost 
            if (type === NEXT_POST) {
                newPost = posts[currentIndex + 1]
            }
            if (type === PREV_POST) {
                newPost = posts[currentIndex - 1]
            }

            setIsPlaying(true)
            setCurrentPost(newPost)
        }
    }
    const getVideoRefById = (postId) => {
        return videoRefs.current[postId]
    }
    const scrollPostIntoView = useCallback((post) => {
        const videoRef = getVideoRefById(post.id)
        if(videoRef){
            videoRef.scrollIntoView({block: "center"})
        }
    },[])
    useEffect(() => {
        if (!currentPost) return
        scrollPostIntoView(currentPost)
        window.history.replaceState(null,document.title,getUrl(currentPost))
    },[currentPost , scrollPostIntoView])

    const doingLikes = useRef([])

    const handleToggleLike = (post) => {
        if(isAuthenticated) {
            if(doingLikes.current.includes(post.id)) return
            doingLikes.current.push(post.id)
    
            let apiPath = `/api/posts/${post.id}/like`
            if(post.is_liked) apiPath = `/api/posts/${post.id}/unlike`
            axios.post(apiPath)
                .then((res) =>{
                    const index = posts.findIndex(item => item.id === post.id)
                    const newPost = PostEntity.create(res.data)
                    setPosts(oldPosts => {
                        const newPosts = oldPosts.slice(0)
                        newPosts.splice(index, 1, newPost)
                        return newPosts
                    })
                    if(currentPost) {
                        // console.log(newPost)
                        setCurrentPost(newPost)
                    }
                })
                .catch(err => {
                    console.log(err)
                })
                .finally(() => {
                    const index = doingLikes.current.indexOf(post.id)
                    doingLikes.current.splice(index, 1)
                })
        }else {
            modal.setModal(LOGIN_MODAL)
        }
    }
   
    const doingFollow = useRef([])
    const handleToggleFollow = (account) => {
        if(isAuthenticated) {
            if(doingFollow.current.includes(account.id)) return 
            doingFollow.current.push(account.id)
            let apiFollow = `/api/users/${account.id}/follow`
            if(account.is_followed)
                apiFollow = `/api/users/${account.id}/unfollow`
            axios.post(apiFollow)
                .then(res => {
                    const newUser = res.data
    
                    const newPosts = posts.slice(0)
                    newPosts.forEach(post => {
                        if(post.user_id === newUser.id) {
                            post.user = newUser
                        }
                    })
                    setPosts(newPosts)
    
                })
                .catch (err => {
                    console.log(err)
                })
                .finally (() => {
                    const index = doingFollow.current.indexOf(account.id)
                    doingFollow.current.splice(index, 1)
                })
        } else {
            modal.setModal(LOGIN_MODAL)
        }
    }
    const handleClickComment = (post) => {
        if(isAuthenticated) {
            handleShowDetail(post)
        }else {
            modal.setModal(LOGIN_MODAL)
        }
    }
    const handleClickShare = (type , post) => {
        const postUrl = `/@${post.user.nickname}/video/${post.uuid}`
        const currentURLPost  = `${window.location.href}${postUrl}`
        if (type === 'facebook')
            window.open(config.socials.shares.facebook(currentURLPost))
        if (type === 'twitter')
            window.open(config.socials.shares.twitter(currentURLPost))
        if (type === 'whatsapp')
            window.open(config.socials.shares.whatsapp(currentURLPost))

    }
    const headleOnClickCopy = (link) => {
        navigator.clipboard.writeText(link)
    }
    return (
        <>
            {postDetail && (
                <>
                    <Post
                        key={postDetail.id}
                        data={postDetail}
                        type={type}
                        isMuted={isMuted}
                        isWaypoint
                        isPlaying={checkPlaying(postDetail)}
                        onWaypointEnter = {handleWaypointEnter}
                        getVideoRef={handleVideoRef}
                        stopWhenPause={stopWhenPause.current}
                        onShowDetail={() => {
                            handleShowDetail(postDetail);
                        }}
                        onTogglePlay = {handleTogglePlay}
                        onToggleMute = {handleToggleMute}
                        onToggleLike = {handleToggleLike}
                        onToggleFollow = {handleToggleFollow}
                        onClickShare = {handleClickShare}
                        onClickCopy = {headleOnClickCopy}
                    />
                   <BrowseHeading title="Browse more For You videos"/>
                </>
            )}
            
            {posts.map(post => (
                    <Post
                        key={post.id}
                        data={post}
                        type={type}
                        isMuted={isMuted}
                        isWaypoint
                        isPlaying={checkPlaying(post)}
                        onWaypointEnter = {handleWaypointEnter}
                        getVideoRef={handleVideoRef}
                        stopWhenPause={stopWhenPause.current}
                        onShowDetail={() => {
                            handleShowDetail(post);
                        }}
                        onTogglePlay = {handleTogglePlay}
                        onToggleMute = {handleToggleMute}
                        onToggleLike = {handleToggleLike}
                        onToggleFollow = {handleToggleFollow}
                        onClickComment = {handleClickComment}
                        onClickShare = {handleClickShare}
                        onClickCopy = {headleOnClickCopy}

                    />
            ) 
            )}
            {currentPost && (
                <PostDetailModal
                    data = {currentPost} 
                    videoId = {currentPost?.id}
                    currentTime = {currentTime.current}
                    isPlaying={isPlaying}
                    onToggleMute = {handleToggleMute}
                    onTogglePlaying = {handleToggleModalPlaying}
                    isMuted={isMuted}
                    onRequestClose={handleCloseDetailPost} 
                    onPrevVideo = {createChangePost(PREV_POST)}
                    onNextVideo = {createChangePost(NEXT_POST)}
                    showNext = {getCurrentIndex() < posts.length - 1}
                    showPrev = {getCurrentIndex() > 0}
                    onToggleLike = {handleToggleLike}
                    onToggleFollow = {handleToggleFollow}
                    onClickShare = {handleClickShare}
                />
                )}
        </>
        
    )
}

export default Home

