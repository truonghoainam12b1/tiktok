import { MessageComponent ,
         MessageItem ,
         MessageList ,
         MessageDetail ,
         MessageDetailItem
      } from '~/components/Message'

function Message () {
   return(
      <MessageComponent>
         <MessageList>
            <MessageItem/>
         </MessageList>
         <MessageDetail>
            <MessageDetailItem/>
         </MessageDetail>
      </MessageComponent>
   )
}

export default Message