import { useState , useEffect , useContext} from 'react'
import axios from 'axios'
import { useHistory , useLocation } from 'react-router-dom'

import SearchAccount from '~/entities/AccountSearch'
import HeaderComponent from '~/components/Header'
import { useDebounce } from'~/hook'
import config from '~/config'
import { ModalContext , LOGIN_MODAL } from '~/modules/ModalProvider'
// import UserContext from '~/contexts/UserContext'

function Header() {

   const [ searchValue , setSearchValue ] = useState('')
   const [ searchResults , setSearchResults ] = useState([])
   const [ searching , setSearching ] = useState(false)
   const [ visibleSearch , setVisibleSearch ] = useState(null)
   const history = useHistory()
   const location = useLocation()
   
   const model = useContext(ModalContext)

   useDebounce(() => {
      if(!searchResults) {
         return setSearchResults([])
      }
      setSearching(true)
      axios.get('/api/users/search?type=less&q=' + searchValue)
         .then(res => {
            setSearchResults(SearchAccount.createFromList(res.data))
            setSearching(false)
         })
         .catch(err => {
            setSearchResults([])
         })

   },800,[searchValue])
   
   useEffect(() => {
      if (!searchValue)
          return setSearchResults([])
  }, [searchValue])

   useEffect(() => {
      if(!location.pathname.includes(config.routes.search)) {
         setSearchValue('')
      }
   },[location.pathname])

   const handleClickLogin = () => {
      model.setModal(LOGIN_MODAL)
   }
   const handleClickUpload = () => {
      history.push(`/upload`)
   }
   const handleClickMessage = () => {
      history.push('/message')
   }
   
   const handleSearchChange = (e) => {
      setSearchValue(e.target.value)
   }
   const handleSearchClear = () => {
      setSearchValue('')
   }
   const handleViewAllSearchResult = () => {
      setVisibleSearch(false)
      history.push(`${config.routes.search}?q=${searchValue}`)
   }
   const handleResultItem = (nickname) => {
      setSearchValue('')
      setSearchResults([])
      history.push(`/@${nickname}`)
   }
   const handleOnClickLogOut = () => {
         axios.post('/api/auth/logout') 
            .then((res) => {
               window.localStorage.removeItem('token')
               window.location.reload()
            })
            .catch((err) => {
               console.log(err)
            })
   }
   const handleOnClickViewProfile = (currentUser) => {
         history.push(`/@${currentUser}`)
         
   }
   const handleOnFocusSearch = () => {
      setVisibleSearch(true)
   }
   const handleOnClickOutside = () => {
      setVisibleSearch(false)
   }
   return(
      <>
         <HeaderComponent
            isSearching={searching}
            onUpload={handleClickUpload}
            onMessage={handleClickMessage}
            onLogin={handleClickLogin}
            onSearchClear={handleSearchClear}
            onSearchChange={handleSearchChange}
            visibleSearch={visibleSearch}
            searchValue={searchValue}
            searchResults={searchResults}
            onViewAllSearchResult={handleViewAllSearchResult}
            onClickResultItem={handleResultItem}
            onClickLogOut={handleOnClickLogOut}
            onClickViewProfile={handleOnClickViewProfile}
            onFocusSearch={handleOnFocusSearch}
            onClickOutside={handleOnClickOutside}
         />
      </>
   )
}

export default Header
