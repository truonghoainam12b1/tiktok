import { useState } from 'react'
import axios from 'axios'

import Button from '~/packages/namth-button'
import {Wrapper ,
         MethodItem,
         Footer ,} from '~/components/Auth'
import TextInput from '~/packages/namth-textInput'
import qrImg from '~/assets/img/QR_Code.svg'
import userImg from '~/assets/img/userImg.png'
import facebookImg from '~/assets/img/facebookImg.png'
import googleImg from '~/assets/img/GoogleImg.png'
import icloudImg from '~/assets/img/icloud.svg'
import intagramImg from '~/assets/img/intagram.png'
import katalkImg from '~/assets/img/kakaotalk.svg'
import lineImg from '~/assets/img/line.png'
import twitterImg from '~/assets/img/twitter.png'
import Toast from '~/components/Toast'
const FORM_EMAIL_PASSWORD = 'FORM_EMAIL_PASSWORD' 

function Login({
   onSwitchRegister = () => {},
   onSuccess = () => {},
}) {
   const [ email , setEmail ] = useState('')
   const [ password , setPassword ] = useState('')
   const [ forms , setForms ] = useState([])
   const [ errors , setErrors ] = useState({})

   const pushForm = (form) => {
      setForms([...forms , form])
   }

   const getCurrentForm = () => {
      if (!forms.length) return null
      return forms[forms.length - 1]
   }
   const handleLogin = () => {
      axios.post('/api/auth/login',{
         email , password
      })
         .then(res => {
            window.localStorage.setItem('token' , res.meta.token)
            onSuccess()
         })
         .catch((err) => {
               switch(err.response.status) {
                  case 422:
                     const resErrors = {}
                     Object.keys(err.response.data.errors).forEach(field => {
                           resErrors[field] = err.response.data.errors[field][0]
                     })
                     setErrors(resErrors)
                     break;
                  case 401:
                     setErrors({
                        ...errors,
                        password: 'Email or password is incorrect'
                     })
                     break;
                  default:
                     setErrors({
                        ...errors,
                        password:'An error has occurred, please contact admin@gmail.com'
                     })
               }
         })
   }
   const getFormTitle = () => {
      const currentForm = getCurrentForm() 
      switch (currentForm) {
         case FORM_EMAIL_PASSWORD: 
            return 'Log in'
         default : 
            return 'Log in with TikTok'
      }
   }
   const handleBackForm = () => {
      const formClone = forms.slice(0)
      formClone.pop()
      setForms(formClone)
   }
   
   const currentForm = getCurrentForm()
   return(
      <Wrapper
            heading = {getFormTitle()}
            showBackBtn = {!!currentForm}
            onBack = {handleBackForm}
            renderFooter={() => (
               <Footer
                  text="Don't have an account?"
                  actionTitle="Sign up"
                  onAction={onSwitchRegister}
               />
            )}

         >
            {currentForm === FORM_EMAIL_PASSWORD ? (
               <>
                  <TextInput
                     label = 'Email'
                     type='text'
                     placeholder="Email or username"
                     value={email}
                     outoFocus
                     message = {errors.email}
                     onChange = {(e) => {
                        setEmail(e.target.value)
                        setErrors({
                           ...errors,
                           email: null
                        })
                     }}
                  />
                  <TextInput
                     label = 'Password'
                     type = 'password'
                     placeholder="Password"
                     value={password}
                     message = {errors.password}
                     onChange = {(e) => {
                        setPassword(e.target.value)
                        setErrors({
                           ...errors,
                           password: null
                        })
                     }}
                  />
                 
                     <Button
                        size='l'
                        style={styles.loginBtn}
                        disabled={!email || !password}
                        onClick={handleLogin}
                     >
                        Log in
                     </Button>
               </>
            ) : (
               <>
                  <MethodItem
                     title='Use QR Code'
                     image={qrImg}
                     onClick={() => {}}
                  />
                  <MethodItem
                     title='User phone / email / username'
                     image={userImg}
                     onClick={() => {
                        pushForm(FORM_EMAIL_PASSWORD)
                     }}
                  />
                  <MethodItem
                     title='Log in with Facebook'
                     image={facebookImg}
                     onClick={() => {}}
                  />
                  <MethodItem
                     title='Log in with Google'
                     image={googleImg}
                     onClick={() => {}}
                  />
                  <MethodItem
                     title='Log in with Twitter'
                     image={twitterImg}
                     onClick={() => {}}
                  />
                  <MethodItem
                     title='Log in with LINE'
                     image={lineImg}
                     onClick={() => {}}
                  />
                  <MethodItem
                     title='Log in with Kakaotalk'
                     image={katalkImg}
                     onClick={() => {}}
                  />
                  <MethodItem
                     title='Log in with iCloud'
                     image={icloudImg}
                     onClick={() => {}}
                  />
                  <MethodItem
                     title='Log in with Instagram'
                     image={intagramImg}
                     onClick={() => {}}
                  />
               </>
            )}
         </Wrapper>
   )
}
const styles = {
      loginBtn: {
      width: '100%',
      marginTop: 32,
      fontWeight: 'bold' 
   }
}

export default Login

