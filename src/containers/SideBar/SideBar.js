import { useContext } from 'react'
import { useSelector } from 'react-redux'

import { selectors as UserSelectors } from '~/state/user'
import AccountList from './AccountList'
import SidebarComponent, {
    TopSidebar,
    Footer
} from '~/components/Sidebar'
import UserContext from '~/contexts/UserContext'
import { ModalContext, LOGIN_MODAL } from '~/modules/ModalProvider'


function Sidebar() {
    const modal = useContext(ModalContext)
    const isAuthenticated = useSelector(UserSelectors.isAuthenticated)

    const handleClickLogin = () => {
        modal.setModal(LOGIN_MODAL)
    }
    
    // const handleClickUpload = () => {
    //   // setModal(REGISTER_MODAL)
    // }
    // const handleCloseModal = () => {
    //     modal.setModal(null)
    // }

    return(
        <SidebarComponent>
            <TopSidebar 
                onLogin={handleClickLogin}
            />
            {isAuthenticated ? (
                        <>
                            <AccountList
                                heading="Suggested accounts"
                                apiPath="/api/users/suggested"
                            />

                            <AccountList
                                heading="Your top accounts"
                                apiPath="/api/me/followings"
                                type="topAccount"
                            />
                        </>
                    ) : (
                        <AccountList
                            heading="Suggested accounts"
                            apiPath="/api/users/suggested"
                        />
                    )}
            

            <Footer/>
        </SidebarComponent>
    )
}

export default Sidebar

