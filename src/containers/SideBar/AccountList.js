import { useEffect , useState} from 'react'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

import { AccountList as AccountListComponent } from "~/components/Sidebar";

function AccountList({ heading = '', apiPath = '',type}) {
    const [isExpanded, setIsExpanded] = useState(false);
    const [accounts, setAccounts] = useState([]);
    const [pagination, setPagination] = useState({
        currentPage: 1,
        total: 0,
        totalPages: 0,
        perPage: 0,
    });
    const history = useHistory()

    useEffect(() => {
        axios
            .get(`${apiPath}?page=${pagination.currentPage}`)
            .then((res) => {
                setAccounts([...accounts, ...res.data]);
                setPagination({
                    ...pagination,
                    total: res.meta.pagination.total,
                    perPage: res.meta.pagination.per_page,
                    totalPages: res.meta.pagination.total_pages,
                });
            })
            .catch((err) => {
                console.log(err);
            });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pagination.currentPage]);

    const handleSeeToggle = () => {
        setIsExpanded(!isExpanded);
        if (pagination.currentPage >= pagination.totalPages) {
            return;
        }
        setPagination({
            currentPage: pagination.currentPage + 1,
        });
    };

    const handleLoadMore = () => {
        if (
            accounts.length <= pagination.perPage ||
            pagination.currentPage >= pagination.totalPages
        ) {
            return;
        }
        setPagination({
            currentPage: pagination.currentPage + 1,
        });
    };
    const handleShowProfile = (nickname) => {
        history.push(`/@${nickname}`)
    }
    return (
        <AccountListComponent
            showProfile={handleShowProfile}
            data={accounts}
            heading={heading}
            onSeeToggle={handleSeeToggle}
            onLoadMore={handleLoadMore}
            hideSeeBtn={pagination.total <= pagination.perPage}
            isExpanded={isExpanded}
            type={type}
        />
    );
}

export default AccountList;
