import { useEffect } from 'react'
import axios from 'axios'


function LoginProvider( {setIsLogin} ) {
   useEffect(() => {
      axios.get('/api/auth/me')
         .then(res => {
            setIsLogin(res.data)
         })
         .catch(err => {
            console.log(err)
         })
   }, [setIsLogin])
   return null
}

export default LoginProvider