import Account from './Account'

class Following extends Account {
   static type = 'Following'
}
Account.addSubClass(Following)

export default Following
