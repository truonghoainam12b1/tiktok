import { useState } from 'react'
import Picker, { SKIN_TONE_MEDIUM_DARK } from 'emoji-picker-react';

function Emoji ({
   children = null
}) {
   const [chosenEmoji, setChosenEmoji] = useState(null);

   const onEmojiClick = (event, emojiObject) => {
    setChosenEmoji(emojiObject);
   };

   return(
      <div>
      {chosenEmoji ? (
        <span>You chose: {chosenEmoji.emoji}</span>
      ) : (
        <span>No emoji Chosen</span>
      )}
      <Picker 
         onEmojiClick={onEmojiClick} 
         skinTone={SKIN_TONE_MEDIUM_DARK}
         chosenEmoji={chosenEmoji}
         >
         {children}
      </Picker>
    </div>
   )
}

export default Emoji