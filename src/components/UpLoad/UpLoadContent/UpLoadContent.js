
import { IoIosCloudUpload } from 'react-icons/io'
import { BsDot } from 'react-icons/bs'
import { FiAtSign } from 'react-icons/fi'
import { HiHashtag } from 'react-icons/hi'
import { BsExclamationCircle } from 'react-icons/bs'
import { Grid , Row , Column } from '@mycv/mycv-grid'


import Button from '~/packages/namth-button'
import styles from './UpLoadContent.module.scss'

function UpLoadContent({
   nickname = '@hoainam10111999',
   count = '0',
}) {
   return(
      <Grid type="wide" maxWidth={1100}>
         <Row>
            <Column sizeDesktop={12}>
               <div className={styles.wrapper}>
                  <div className={styles.header}>
                     <h3>Upload Video</h3>
                     <p>This video will be published to {nickname} </p>
                  </div>
                  <div className={styles.content}>
                     <div className={styles.file}>
                        <div className={styles.card}>
                           <div className={styles.iconUpload}>
                              <IoIosCloudUpload/>
                           </div>
                           <p className={styles.text_main}>Select video to upload</p>
                           <p className={styles.text_sup}>Or drag and drop a file</p>
                           <ul className={styles.text_list}>
                              <li className={styles.text_item}>
                                 <BsDot className={styles.dot}/>MP4 or WebM
                              </li>
                              <li className={styles.text_item}>
                                 <BsDot className={styles.dot}/>720x1280 resolution or higher
                              </li>
                              <li className={styles.text_item}>
                                 <BsDot className={styles.dot}/>Up to 180 seconds
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div className = {styles.form}>
                        <div className = {styles.container}>
                           <div className={styles.container_text}>
                              <span className={styles.title_font}>Caption</span>
                              <span className={styles.require_font}>{count} / 150</span>
                           </div>
                           <div className={styles.input}>
                              <p className={styles.input_text} contenteditable="true">''</p>
                              <div className={styles.icon}>
                                 <button>
                                    <FiAtSign/>
                                 </button>
                                 <button>
                                    <HiHashtag/>
                                 </button>
                              </div>
                           </div>
                        </div>
                        <div className={styles.form_item}>
                           <p className={styles.title}>Cover</p>
                           <div className={styles.form_item_container}>
                              <div className={styles.form_item_container_cover}></div>
                           </div>
                        </div>
                        <div className={styles.form_item_flex}>
                           <div className={styles.form_item_select}>
                              <p className={styles.title}>Who can view this video</p>
                              <div className={styles.radio_group}>
                                 <label className={styles.radio}>
                                    <input type="radio" name="radio" defaultChecked="checked" value={0}/>
                                    <span className={styles.radio_box}>Public</span>
                                 </label>
                                 <label className={styles.radio}>
                                    <input type="radio" name="radio" value={1}/>
                                    <span className={styles.radio_box}>Friends</span>
                                 </label>
                                 <label className={styles.radio}>
                                    <input type="radio" name="radio" value={2}/>
                                    <span className={styles.radio_box}>Private</span>
                                 </label>
                              </div>
                           </div>
                           <div className={styles.form_item_select}>
                              <p className={styles.title}>Allow users to:</p>
                              <div className={styles.checkbox_group}>
                                 <label className={styles.checkbox}>
                                    <input type="checkbox" name="checkbox" defaultChecked="checked" value='comment'/>
                                    <span className={styles.checkbox_box}>Comment</span>
                                 </label>
                                 <label className={styles.checkbox}>
                                    <input type="checkbox" name="checkbox" defaultChecked="checked" value='duet'/>
                                    <span className={styles.checkbox_box}>Duet / React</span>
                                 </label>
                                 <label className={styles.checkbox}>
                                    <input type="checkbox" name="checkbox" defaultChecked="checked" value='stitch'/>
                                    <span className={styles.checkbox_box}>Stitch</span>
                                 </label>
                              </div>
                           </div>
                        </div>
                        <div className={styles.scheduled}>
                           <p className={styles.scheduled_title}>Schedule video</p>
                           <button className={styles.scheduled_icon}>
                              <BsExclamationCircle/>
                           </button>
                           <div className={[styles.scheduled_switch , styles.on].join(' ')}>
                              <div className={styles.switch}></div>
                           </div>
                        </div>
                        <div className={styles.form_btn}>
                           <Button size='ms' type='normal'>DisCard</Button>
                           <Button size='ms' type='primary'>Post</Button>
                        </div>
                     </div>
                  </div>
               </div>
            </Column>
         </Row>
      </Grid>
     
   )
}

export default UpLoadContent