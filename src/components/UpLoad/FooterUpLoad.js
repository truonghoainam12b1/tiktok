
import { AiFillCaretDown } from 'react-icons/ai';


import styles from './UpLoad.module.scss'
import LogoLight from '~/assets/img/logo-light.svg'
import LogoTextLight from '~/assets/img/logo-text-light.svg'



function FooterUpLoad () {
   return(
      <div className={styles.footer}>
         <div className={styles.footer_container}>
            <div className={styles.footer_content}>
               <div className={styles.footer_logo}>
                  <img src={LogoLight} alt='tiktok'/>
                  <img src={LogoTextLight} alt='tiktok'/>
               </div>
               <div className={styles.footer_content_column}>
                  <h2>Company</h2>
                  <span><a href = "nam">About</a></span>
                  <span><a href = "nam">Newsroom</a></span>
                  <span><a href = "nam">Contact</a></span>
                  <span><a href = "nam">Careers</a></span>
                  <span><a href = "nam">ByteDance</a></span>
               </div>
               <div className={styles.footer_content_column}>
                  <h2>Programs</h2>
                  <span><a href = "nam">TikTok for Good</a></span>
                  <span><a href = "nam">Newsroom</a></span>
                  <span><a href = "nam">Advertise</a></span>
                  <span><a href = "nam">Developers</a></span>
               </div>
               <div className={styles.footer_content_column}>
                  <h2>Support</h2>
                  <span><a href = "nam">Help Center</a></span>
                  <span><a href = "nam">Safety Center</a></span>
                  <span><a href = "nam">Creator Portal</a></span>
                  <span><a href = "nam">Community Guidelines</a></span>
                  <span><a href = "nam">Transparency</a></span>
               </div>
               <div className={styles.footer_content_column}>
                  <h2>Legal</h2>
                  <span><a href = "nam">TikTok Platform Cookies Policy</a></span>
                  <span><a href = "nam">Intellectual Property Policy</a></span>
                  <span><a href = "nam">Law Enforcement Guidelines</a></span>
                  <span><a href = "nam">Privacy Policy</a></span>
                  <span><a href = "nam">Terms of Service</a></span>
               </div>
            </div>
            <div className={styles.footer_bottom}>
               <div className={styles.language_selection}>
                  <span>English</span>
                  <button>
                     <AiFillCaretDown/>
                  </button>
               </div>
               <div className={styles.copyright}>
                  © 2021 TikTok
               </div>
            </div>
         </div>
      </div>
   )
}

export default FooterUpLoad