import styles from './UpLoad.module.scss'

function UpLoad ({ children = null}) {
   return(
      <div className={styles.wrapper}>
         {children}
      </div>
   )
}

export default UpLoad