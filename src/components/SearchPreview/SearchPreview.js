import styles from './SearchPreview.module.scss'

function SearchPreview({
   children = null,
   searchValue ='',
   onViewAll = () => {},
}) {
   return(
      <div className={styles.wrapper}>
         {children}
         <div className={styles.viewALL} onClick={onViewAll}>
            View all results for "{searchValue}"
         </div>
      </div>
   )
}

export default SearchPreview