
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'

import styles from './SearchPreview.module.scss'

function ResultItem ({
   title = '',
   tick = false ,
   description = '',
   onClick = () => {},
}) {
   return(
      <Link  className={styles.item} onClick={() => onClick(title)}>
         <span className={styles.title}>{title}</span>
         {tick ? <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/> : ''}
         <p className={styles.description}>{description}</p>
      </Link>
   )
}

export default ResultItem
