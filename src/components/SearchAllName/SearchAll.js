
import SearchAllItem from './SearchAllItem'
import styles from './SearchAll.module.scss'


import { FiChevronDown } from 'react-icons/fi';


function SearchAll ({
    searchAll,
    onClick = () => {},
    onLoadMore = () => {},
}) {
    return (
        <div className={styles.wrapper}>
            {searchAll.map(item => <SearchAllItem 
                key={item?.id}
                data = {item}
                onClick={onClick}
            />)}
            {searchAll.length === 10 && (
                <div className={styles.loadMore} onClick={onLoadMore}>
                    <span>loadMore</span>
                    <FiChevronDown className={styles.chevronDown}/>
                </div>
            )}
        </div>
    )
}

export default SearchAll
