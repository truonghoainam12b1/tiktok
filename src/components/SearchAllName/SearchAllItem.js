

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle} from '@fortawesome/free-solid-svg-icons'

import styles from './SearchAll.module.scss'

function SearchAllItem ({
    data = [],
    onClick = () => {},
}) {
    console.log(data)
    return(
        <div className={styles.itemBox} onClick={() => {onClick(data?.nickname)}}>
            <div className = {styles.avatar}>
                <img src={data?.avatar} alt={data?.nickname}/>
            </div>
            <div className = {styles.itemInfo}>
                <div className = {styles.nickname}>
                    <span>{data?.nickname}</span>
                    <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/>
                </div>
                <div className = {styles.subtitle}>
                    <span className = {styles.fullname}>{data?.full_name} -</span>
                    <div className = {styles.count}>
                        <span className = {styles.countFollower}>{data?.followers_count}</span>
                        <span className = {styles.followers}>Followers</span>
                    </div>
                </div>
                <p className = {styles.description}>{data?.bio}</p>
            </div>
        </div>
    )
}

export default SearchAllItem