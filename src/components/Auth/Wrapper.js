import { AiOutlineLeft } from 'react-icons/ai';

import styles from './Auth.module.scss';

function Heading ({
   heading = '',
   children = null,
   showBackBtn = false,
   renderFooter = () => {},
   onBack = () => {},
}
) {
   return(
      <div className={styles.wrapper}>
         <h2 className={styles.heading}>{heading}</h2>
         {showBackBtn && (
            <button className={styles.backBtn} onClick={onBack}>
               <AiOutlineLeft className={styles.backIcon}/>
            </button>
         )}
         <div className={styles.body}>
            {children}  
         </div>
         {renderFooter()}
      </div>
   )
}

export default Heading