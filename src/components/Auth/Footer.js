
import { BiHelpCircle } from 'react-icons/bi';
import styles from './Auth.module.scss'

function Footer({
   text = '',
   onAction = () => {},
   actionTitle = '',
}) {
   return(
      <div className={styles.footer}>
         <div className={styles.footerBox}>
            <span>{text}</span>
            <span className={styles.footerLink} onClick={onAction}>{actionTitle}</span>
         </div>
         <div className={styles.btnHelp}>
            <BiHelpCircle/>
         </div>
      </div>
   )
}

export default Footer