import styles from './Auth.module.scss'

function MethoudItem({
   onClick = () => {} ,
   title = '',
   image = null,
}) {
   return (
      <div className={styles.methodItem} onClick={onClick}>
         <div className={styles.methodIcon}>
            <img src={image} className={styles.img} alt=''/>
         </div>
         <div className={styles.methodBody}>
            <div className={styles.title}>{title}</div> 
         </div>
      </div>
   )
}

export default MethoudItem
