import {
    BrowserRouter as Router,
    Route,
    Switch,
} from 'react-router-dom'
import { Grid , Row , Column} from '@mycv/mycv-grid'
import { useState } from 'react'

import HomeContainer from '~/containers/Home'
import SidebarContainer from '~/containers/SideBar'
import PostDetailContainer from '~/containers/PostDetail'
import FollowingContainer from '~/containers/Following'
import HeaderContainer from '~/containers/Header'
import ProfileContainer from '~/containers/Profile'
import SearchAllContainer from '~/containers/SearchAll'
import UpLoadContainer from '~/containers/UpLoad'
import PageNotFoundComponent from '~/components/PageNotFound'
import config from '~/config'
// import UserContext from '~/contexts/UserContext'
// import UserProvider from '~/containers/UserProvider'
import MessageContainer from '~/containers/Message'
import ModalProvider , { ModalContext } from'~/modules/ModalProvider'
import { Provider as UserProvider } from '~/state/user'


function App() {
    // const [ currentUser , setCurrentUser ] = useState(null)
    const [ modal , setModal ] = useState({
        current: null,
        setModal: current => setModal(prev => ({...prev , current}))
    })
    const renderWithSidbar = Component => {
        return () => (
        <Grid type="wide" maxWidth={1100}>
            <Row>
                <Column size={0} sizeTablet={4} sizeDesktop={3}>
                    <SidebarContainer/>
                </Column>
                <Column size={12} sizeTablet={8} sizeDesktop={9}>
                    <Switch>
                        <Component/>
                    </Switch>
                </Column>
            </Row>  
        </Grid>  
        )
    }
    return (
        <ModalContext.Provider value={modal}>
            {/* <UserContext.Provider value={currentUser}> */}
                <Router>
                    <Grid>
                        {/* <UserProvider setCurrentUser={setCurrentUser}/> */}
                        <UserProvider />
                        <ModalProvider setModal={setModal}/>
                        <HeaderContainer />
                        <Switch>
                            <Route exact path={config.routes.upLoad} component={UpLoadContainer}/>
                            <Route exact path={config.routes.message} component={MessageContainer}/>
                            <Route exact path={config.routes.home} component={renderWithSidbar(HomeContainer)}/>
                            <Route exact path={config.routes.postDetail} component={renderWithSidbar(PostDetailContainer)} />
                            <Route exact path={config.routes.following} component={renderWithSidbar(FollowingContainer)} />
                            <Route exact path={config.routes.search} component={renderWithSidbar(SearchAllContainer)} />
                            <Route exact path={config.routes.profile} component={renderWithSidbar(ProfileContainer)} />
                            <Route component={PageNotFoundComponent} />
                        </Switch>
                        
                    </Grid>
                </Router>
            {/* </UserContext.Provider> */}
        </ModalContext.Provider>
    )
}

export default App
