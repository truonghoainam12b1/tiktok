import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'

import styles from "./AccountPreview.module.scss";
import Button from "~/packages/namth-button";

function AccountPreview({
   avatar = '',
   nickname = '',
   tick = false ,
   fullname = '',
   followers = '',
   likes = '',
   bio = '',
   isFollow = false,
   showBio = true,
   showProfile =() => {},
}) {
   return (
      <div className={styles.wrapper}>
         <img className={styles.avatar} src={avatar} alt=""  onClick={() => showProfile(nickname)} />
         <div className={styles.title}>
            <span className={styles.nickname}  onClick={() => showProfile(nickname)}>{nickname}</span>
            {tick && <FontAwesomeIcon className={styles.checkCircleIcon}icon={faCheckCircle}/>}
         </div>
         <p className={styles.fullname}>{fullname}</p>
         <div className={styles.analytics}>
            <span className={styles.analyticsItem}>
               <strong>{followers}</strong>Followers
            </span>
            <span className={styles.analyticsItem}>
               <strong>{likes}</strong>Likes
            </span>
         </div>
         <div className={styles.btnFollow}>
            <Button type='border' size='m' >
               {isFollow ? 'Following' : 'Follow'}
            </Button>
         </div>
         {showBio && (
            <div className={styles.description}>
               <p className={styles.bio}>{bio}</p>
            </div>
         )}
      </div>
   );
}

export default AccountPreview;
