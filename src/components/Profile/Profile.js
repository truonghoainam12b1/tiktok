
import styles from'./Profile.module.scss'
import VideoList from './VideoList'
function Profile({ 
    children = null,
    profile,
    isPlaying,
    onMouseEnter = () => {},
    onShowDetail = () => {},
}) {
    
    return(
        <div className={styles.wrapper}>
            {children}
            <div className={styles.layoutMain}>
                    {profile?.posts?.map(video => <VideoList
                            key={video.id}
                            data={video}
                            isPlaying={isPlaying(video)}
                            onMouseEnter={onMouseEnter}
                            onShowDetail={onShowDetail}
                    />)}
                </div>
        </div>
    )
}

export default Profile
