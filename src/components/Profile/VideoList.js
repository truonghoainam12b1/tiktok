
import { useRef , useEffect} from 'react'

import styles from './Profile.module.scss'
import { BsPlay } from 'react-icons/bs';

function VideoList({ 
    data = [],
    isPlaying = false,
    onMouseEnter = () => {},
    onShowDetail = () => {},
}) {
    const videoRef = useRef(null)
    useEffect(() => {
        if(!videoRef) return
        if(isPlaying) {
            videoRef.current.play()
        } else {
            videoRef.current.pause()
            videoRef.current.currentTime = 0
        }
    }, [isPlaying])
    

    return(
            <div className={styles.videoItem} 
                onMouseEnter={() => onMouseEnter(data)}
                onClick={() => onShowDetail(data)}
            >
                <video 
                    src={data.file_url}
                    className={styles.video}
                    muted
                    ref={videoRef}
                />
                <div className={styles.videoView}>
                    <BsPlay className={styles.bsplay}/>
                    <span>{data.views_count}</span>
                </div>
            </div>
    )
}
export default VideoList
