
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { HiOutlineDotsHorizontal } from 'react-icons/hi'
import { GrUserExpert } from 'react-icons/gr'
import { useSelector } from 'react-redux'

import { selectors as UserSelectors } from '~/state/user'
import styles from './Profile.module.scss'
import Button from '../../packages/namth-button'
import Tooltip from '~/components/Tooltip'

function ProfileInfo({
    data = [] ,
    tapLike ,
    onClickMessage = () => {},
    onToggleFollow = () => {},
}) {
    const currentUser = useSelector(UserSelectors.getCurrentUser)
    return(
        <div className={styles.profileInfo}>
            <div className={styles.shareInfo}>
                <div className={styles.avatar}>
                    <img src={data?.avatar} alt=''/>
                </div>
                <div className={styles.shareTitle}>
                    <div>
                        <span className={styles.nickname}>{data?.nickname}</span>
                        {data?.tick && <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/>}
                    </div>
                    <p className={styles.fullname}>{data?.full_name}</p>
                    <div>
                        {currentUser?.id === data?.id || (
                            data?.posts[0].user.is_followed ? 
                            (   <div className={styles.is_followed}>
                                    <Button type='border' size='ss' onClick={onClickMessage}>Message</Button>
                                    <Tooltip content="Unfollow" interactive>
                                        <div className={styles.unfollow} onClick={onToggleFollow}>
                                            <GrUserExpert/>
                                        </div>
                                    </Tooltip>
                                </div>    ) :  
                            (   <Button type='primary' size='ssm' onClick={onToggleFollow}>Follow</Button>   )
                        )}
                    </div>
                </div>
            </div>
            <div className={styles.countInfo}>
                <div className={styles.number}>
                    <span className={styles.count}>{data.followings_count}</span>
                    <span className={styles.countTitle}>Following</span>
                </div>
                <div className={styles.number}>
                    <span className={styles.count}>{data.followers_count}</span>
                    <span className={styles.countTitle}>Followers</span>
                </div>
                <div className={styles.number}>
                    <span className={styles.count}>{data.likes_count}</span>
                    <span className={styles.countTitle}>Likes</span>
                </div>
            </div>
            <p className={styles.description}>{data?.bio}</p>
            <div className={styles.contactLink}>
                <a className={styles.shareLink} target="_blank" rel="noreferrer"  href={data?.facebook_url} >{data?.facebook_url}</a>
                <a className={styles.shareLink} target="_blank" rel="noreferrer"  href={data?.youtube_url}>{data?.youtube_url}</a>
                <a className={styles.shareLink} target="_blank" rel="noreferrer"  href={data?.twitter_url}>{data?.twitter_url}</a>
                <a className={styles.shareLink} target="_blank" rel="noreferrer"  href={data?.instagram_url}>{data?.instagram_url}</a>
            </div>
            <HiOutlineDotsHorizontal className={styles.reportIcon}/>
            <div className={styles.videoTap}>
                <p className={styles.videos}>
                    <span>Videos</span>
                </p>
                <p className={styles.likes}>
                    <span>Likes</span>
                </p>
                <p className={[styles.line , tapLike ? styles.like : ''].join('')}></p>
            </div>
        </div>
    )
}
export default ProfileInfo
