
import { AiOutlineUser ,  AiOutlineSetting } from 'react-icons/ai'
import { IoMdAnalytics , IoIosHelpCircleOutline , IoIosLogOut} from 'react-icons/io'
import { IoLanguageOutline } from 'react-icons/io5'

import styles from './OptionPreview.module.scss'

function OptionPreview ({
   onClickLogOut = () => {},
   onClickViewProfile = () => {},
}) {
   return(  
      <div className={styles.wrapper}>
         <div className={styles.item} onClick={onClickViewProfile}>
            <div className={styles.icon}>
               <AiOutlineUser/>
            </div>
            <div className={styles.title}>
               View Profile
            </div>
         </div>
         <div className={styles.item}>
            <div className={styles.icon}>
               <IoMdAnalytics/>
            </div>
            <div className={styles.title}>
               View Analytics
            </div>
         </div>
         <div className={styles.item}>
            <div className={styles.icon}>
               <AiOutlineSetting/>
            </div>
            <div className={styles.title}>
               Settings
            </div>
         </div>
         <div className={styles.item}>
            <div className={styles.icon}>
               <IoLanguageOutline/>
            </div>
            <div className={styles.title}>
               English
            </div>
         </div>
         <div className={styles.item}>
            <div className={styles.icon}>
               <IoIosHelpCircleOutline/>
            </div>
            <div className={styles.title}>
               Feedback and Help
            </div>
         </div>
         <div className={[styles.item , styles.logOut].join(' ')} onClick={onClickLogOut}>
            <div className={styles.icon}>
               <IoIosLogOut/>
            </div>
            <div className={styles.title}>
               Logout
            </div>
         </div>
      </div>
   )
}

export default OptionPreview