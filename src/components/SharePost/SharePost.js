
import { FaWhatsappSquare , FaFacebook , FaPinterest , FaReddit} from 'react-icons/fa';
import { AiFillTwitterCircle } from 'react-icons/ai';
// import { RiCodeBoxFill } from 'react-icons/ri';
import { IoIosLink } from 'react-icons/io';
// import { CopyToClipboard } from 'react-copy-to-clipboard'
import Toast from '~/components/Toast'
import styles from './SharePost.module.scss'

function SharePost ({
   post ,
   onClickShare = () => {},
   onClickCopy = () => {},
}) {
   const path = window.location.host
   return(
      <div className={styles.wrapper}>
         {/* <div className={styles.shareBtn}>
            <RiCodeBoxFill className={[styles.icon , styles.code].join(' ')}/>
            <p className={styles.title}>Embed</p>
         </div> */}
         <div className={styles.shareBtn} onClick={() => onClickShare('whatsapp',post)}>
            <FaWhatsappSquare className={[styles.icon , styles.whatsapp].join(' ')}/>
            <p className={styles.title}>Share to Whatsapp</p>
         </div>
         <div className={styles.shareBtn} onClick={() => onClickShare('twitter',post)}>
            <AiFillTwitterCircle className={[styles.icon , styles.twitter].join(' ')}/>
            <p className={styles.title}>Share to Twitter</p>
         </div>
         {/* <div className={styles.shareBtn} onClick={() => onClickShare('facebook',post)}>
            <FaPinterest className={[styles.icon , styles.pinterest].join(' ')}/>
            <p className={styles.title}>Share to Pinterest</p>
         </div> */}
         <div className={styles.shareBtn} onClick={() => onClickShare('reddit',post)}>
            <FaReddit className={[styles.icon , styles.reddit].join(' ')}/>
            <p className={styles.title}>Share to Reddit</p>
         </div>
         <div className={styles.shareBtn} onClick={() => onClickShare('facebook',post)}>
            <FaFacebook className={[styles.icon , styles.facebook].join(' ')}/>
            <p className={styles.title}>Share to Facebook</p>
         </div>
         {/* <Toast message='Copied'> */}
            <div className={styles.shareBtn}
                  onClick={() => onClickCopy(`${path}/${post.user.nickname}/video/${post.uuid}`)}
            >
               <IoIosLink className={[styles.icon , styles.link].join(' ')}/>
               <p className={styles.title}>Copy Link</p>
            </div>
         {/* </Toast> */}
         
      </div>
   )

}

export default SharePost
