
import { BsThreeDots } from 'react-icons/bs'

import styles from './MessageItem.module.scss'

function MessageItem ({
   avatar = 'https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/c22a7891a16de24367c3576e9b7defcf.jpeg?x-expires=1620489600&x-signature=czO%2FxEpyvr4ZE%2Bgyc%2Bc7oleZyb0%3D',
   fullname = 'Thu Thuy Le' ,
   message = 'Hôm nay em có phải đi làm ko hả em' ,
   time = '5-15 PM' ,
}) {
   return(
      <div className={styles.wrapper}>
         <div className={styles.item}>
            <div className={styles.img}>
               <img src={avatar} alt=''/>
            </div>
            <div className={styles.info}>
               <h3 className={styles.name}>{fullname}</h3>
               <div className={styles.content}>
                  <span className={styles.message}>{message}</span>
                  <span className={styles.time}>{time}</span>
               </div>
            </div>
            <button className={styles.options}>
               <BsThreeDots/>
            </button>
         </div>
      </div>
   )
}

export default MessageItem