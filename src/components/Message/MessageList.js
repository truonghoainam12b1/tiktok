import { IoSettingsOutline } from 'react-icons/io5'
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react'

import styles from './Message.module.scss'

function MessageList(
   {children}
) {
   return(
      <div className={styles.commentBox}>
         <div className={styles.commentHeader}>
            <span className={styles.title}>
               Messages
            </span>
            <button className={styles.setting}>
               <IoSettingsOutline/>
            </button>
         </div>
         <OverlayScrollbarsComponent
            className={styles.commentList}
            options={{
               scrollbars: {
               autoHide: 'leave',
               autoHideDelay: 0
                   }
           }}        
         >
            {children}
         </OverlayScrollbarsComponent>
      </div>
   )
}

export default MessageList