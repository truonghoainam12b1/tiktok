import styles from './MessageDetail.module.scss'
import { OverlayScrollbarsComponent } from 'overlayscrollbars-react'
import { IoHappyOutline } from 'react-icons/io5'
import { RiSendPlaneFill } from 'react-icons/ri'

 
function MessageDetail ({ 
   avatar = 'https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/c22a7891a16de24367c3576e9b7defcf.jpeg?x-expires=1620489600&x-signature=czO%2FxEpyvr4ZE%2Bgyc%2Bc7oleZyb0%3D', 
   fullname = 'Thu Thuy Le' ,
   nickname = '@thuthuyle' ,
   children = null ,
}) {
   return(
      <div className={styles.wrapper}>
         <div className={styles.header}>
            <div className={styles.avatar}>
               <img src={avatar} alt=''/>
            </div>
            <div className={styles.info}>
               <h2 className={styles.fullname}>{fullname}</h2>
               <p className={styles.nickname}>{nickname}</p>
            </div>
         </div>
         <OverlayScrollbarsComponent
            className={styles.content}
            options={{
               scrollbars: {
               autoHide: 'leave',
               autoHideDelay: 0
                   }
           }}        
         >
            {children}
         </OverlayScrollbarsComponent>
         <div className={styles.footer}>
             <div className={styles.editor}>
                <div className={styles.input}>
                  <input type="text" className={styles.input} placeholder="Send a message..."/>
                  <div className={styles.emoji}>
                     <IoHappyOutline/>
                  </div>
                </div>
                <div className={styles.send}>
                  <RiSendPlaneFill/>
                </div>
             </div>
         </div>
      </div>
   )
}

export default MessageDetail
