import { BiDotsHorizontalRounded } from 'react-icons/bi'
import styles from './MessageDetail.module.scss'

function MessageDetailItem ({
   avatar = 'https://p16-sign-sg.tiktokcdn.com/aweme/100x100/tos-alisg-avt-0068/c22a7891a16de24367c3576e9b7defcf.jpeg?x-expires=1620489600&x-signature=czO%2FxEpyvr4ZE%2Bgyc%2Bc7oleZyb0%3D',
   message = 'Hôm nay trời đẹp quá ohffffohffffffHômffHôm nay trời đẹp quá ohffffffHôm nay trời đẹp quá ohffffffHôm nay trời đẹp quá ohffffffHôm nay trời đẹp quá ohffffff' ,
}) {
   return (
      <div className={[styles.item , styles.mySelf].join(' ')}>
         <div className={styles.avatarItem}>
            <img src={avatar} alt= ''/>
         </div>
         <div className={styles.box}>
            <p className={styles.message}>{message}</p>
            <div className={styles.option}>
               <BiDotsHorizontalRounded/>
            </div>
         </div>
      </div>
   ) 
}

export default MessageDetailItem