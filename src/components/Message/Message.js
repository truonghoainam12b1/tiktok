import { IoArrowBackOutline } from 'react-icons/io5'

import styles from './Message.module.scss'

function Message({ children = null }) {
   return(
      <div className={styles.wrapper}>
         <div className={styles.btn_back}>
            <IoArrowBackOutline/>
         </div>
         <div className={styles.main}>
            {children}
         </div>
      </div>
   )
}

export default Message