

import img from "~/assets/img/404.png"
import FooterComponent from "~/components/UpLoad/FooterUpLoad"
import config from "~/config"
import styles from "./PageNotFound.module.scss"

function PageNotFound() {
  return (
    <>
      <div className={styles.wrapper}>
        <h1 className={styles.title}>
          <span>4</span>
          <img className={styles.icon404} src={img} alt='0'/>
          <span>4</span>
        </h1>
        <p className={styles.desc}>Couldn't find this page</p>
        <h3 className={styles.subTitle}>Check out more trending videos on TikTok</h3>
      </div>
      <FooterComponent />
    </>
  )
}

export default PageNotFound
