import Tippy from '@tippyjs/react/headless'

import styles from './Popper.module.scss'
function Popper({
    children,
    render,
    duration=0,
    delay=0,
    minWidth=0,
    maxWidth=0,
    wrapperClassName ='',
    display='block',
    appendTo = "parent",
    onShow,
    ...props
}) {
    return(
        <Tippy 
            {...props}
            delay={delay}
            duration={duration}
            appendTo = {appendTo}
            render={() => (
                <div className={[styles.wrapper , wrapperClassName].join(' ')} style={{ minWidth , maxWidth , display}} >
                    <div className={styles.inner}>
                        {render()}
                    </div>
                </div>
            )}
        >
            <div className={styles.box}>
                {children}
            </div>
        </Tippy>
    )
}

export default Popper
