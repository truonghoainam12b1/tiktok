
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { useSelector } from 'react-redux'

import { selectors as UserSelectors } from '~/state/user'
import styles from "./AccountList.module.scss";
import AccountItem from "./AccountItem";
import Popper from "../../Popper"
import AccountPreview from "../../AccountPreview"
import UserContext from "~/contexts/UserContext"

function AccountList({
    data = [],
    heading = "",
    onSeeToggle = () => {},
    onLoadMore = () => {},
    showProfile = () => {},
    expandedTitle = "See all",
    collapseTitle = "See less",
    isExpanded = false,
    hideSeeBtn = false,
    type = 'suggested'
}) {
    const classNames = [styles.inner]
    const currentUser = useSelector(UserSelectors.getCurrentUser)
    if (!isExpanded) {
        classNames.push(styles.closed)
    }
    return (
        <div className={styles.wrapper}>
            <h3 className={styles.heading}>{heading}</h3>
            <div className={classNames.join(' ')}>
                {data.map((account, index) => {
                    if(type === 'suggested') {
                        return (
                            <Popper
                            key={account.id}
                            placement= 'bottom'
                            interactive
                            duration={[300, 250]}
                            minWidth={300}
                            delay={[600 , 0]}
                            offset={[20, -4]}
                            appendTo={() => document.body}
                            render={() => (
                                <AccountPreview
                                    avatar={account.avatar}
                                    nickname={account.nickname}
                                    tick={account.tick}
                                    fullname={`${account.first_name} ${account.last_name}`}
                                    followers={account?.followers_count}
                                    likes={account?.likes_count}
                                    isFollow={false}    
                                    bio={account?.bio}
                                    showBio={false}
                                    showProfile={showProfile}
                                />
                            )}
                        >
                        {currentUser?.id === account?.id || (
                                    <AccountItem
                                        avatar={account.avatar}
                                        title={account.nickname}
                                        description={`${account.first_name} ${account.last_name}`}
                                        tick={account.tick}
                                        linkTo={`/@${account.nickname}`}
                                        isLast={data.length - 1 === index}
                                        followers={account?.followers_count}
                                        likes={account?.likes_count}
                                        onEnter={onLoadMore}
                                        onLeave={() => {}}
                                    />
                                )}
                        
                        </Popper>
                    )
                    }
                    return (
                        <UserContext.Consumer>
                        {currentUser => (
                            currentUser?.id === account?.id || (
                                <AccountItem
                                    avatar={account.avatar}
                                    title={account.nickname}
                                    description={`${account.first_name} ${account.last_name}`}
                                    tick={account.tick}
                                    linkTo={`/@${account.nickname}`}
                                    isLast={data.length - 1 === index}
                                    followers={account?.followers_count}
                                    likes={account?.likes_count}
                                    onEnter={onLoadMore}
                                    onLeave={() => {}}
                                />
                            )
                        )}
                    </UserContext.Consumer>
                )
                })}
            </div>
            {!hideSeeBtn && (
                <div className={styles.seeAll} onClick={onSeeToggle}>
                    <span>{isExpanded ? collapseTitle : expandedTitle}</span>
                    <FontAwesomeIcon
                        icon={isExpanded ? faChevronUp : faChevronDown}
                        className={styles.chevronDown}
                    />
                </div>
            )}
        </div>
    );
}

export default AccountList;
