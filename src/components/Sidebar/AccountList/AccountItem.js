import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { Waypoint } from 'react-waypoint'
import { Fragment } from 'react'

import styles from './AccountList.module.scss'



function AccountItem ({
    avatar = '',
    title = '' ,
    description = '',
    tick = false,
    linkTo = '',
    isLast = false,
    onEnter = () => {},
    onLeave = () => {}
}) {
    const props = {}
    let Component = Fragment
    if(isLast) {
        Component = Waypoint
        props.onEnter = onEnter
        props.onLeave = onLeave
    }
    return (
            <Component {...props}>
                <Link to={linkTo} className={styles.accountItem} >
                    <div className={styles.avatar}>
                        <img src={avatar} alt='' className={styles.accountAvatar}/>
                    </div>
                    <div className={styles.accountBox}>
                        <span className={styles.accountTitle}>{title}</span>
                        {tick ? <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/> : ''}
                        <p className={styles.accountDesc}>{description}</p>
                    </div>
                </Link>
            </Component>
    )
}

export default AccountItem