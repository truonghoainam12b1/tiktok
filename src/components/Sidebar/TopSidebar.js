import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faUserFriends } from '@fortawesome/free-solid-svg-icons'
import { NavLink  } from 'react-router-dom'
import { useSelector } from 'react-redux'

import { selectors as UserSelectors } from '~/state/user'
import Button from '~/packages/namth-button'
import config from '~/config'
import styles from './Sidebar.module.scss'


function TopSidebar({
    onLogin = () => {},
}) {
    const isAuthenticated = useSelector(UserSelectors.isAuthenticated)
    return (    
        <div className={styles.topSidebar}>
            <NavLink exact to={config.routes.home} className={styles.navItem} activeClassName={styles.active}>
                <FontAwesomeIcon icon={faHome} className={styles.navIcon} />
                <span>For You</span>
            </NavLink>
            <NavLink to={config.routes.following} className={styles.navItem} activeClassName={styles.active}>
                <FontAwesomeIcon icon={faUserFriends} className={styles.navIcon} />
                <span>Following</span>
            </NavLink>
            {!!isAuthenticated || 
                <div>
                    <p className={styles.helpText}>
                        Log in to follow creators, like videos, and view comments.
                    </p>
                    <Button type="border" size="l" onClick={onLogin}>Log in</Button>
                </div>
            }
            
                
            
        </div>
    )
}

export default TopSidebar
