
import Styles from './Footer.module.scss'
function Footer () {
    return (
        <div className={Styles.wrapper}>
            <div className={Styles.item}>
                <a target='_blank' rel="noopener noreferrer" href='https://www.tiktok.com/about?lang=en' className={Styles.link}>About</a>
                <a target='_blank' rel="noopener noreferrer" href='https://newsroom.tiktok.com/vi-vn/' className={Styles.link}>Newrooms</a>
                <a target='_blank' rel="noopener noreferrer" href='https://www.tiktok.com/about/contact?lang=en' className={Styles.link}>Contact</a>
                <a target='_blank' rel="noopener noreferrer" href='https://careers.tiktok.com/' className={Styles.link}>Careers</a>
                <a target='_blank' rel="noopener noreferrer" href='https://www.bytedance.com/en/' className={Styles.link}>ByteDance</a>
            </div>
            <div className={Styles.item}>
                <a target='_blank' rel="noopener noreferrer" href='https://www.tiktok.com/forgood' className={Styles.link}>Tiktok for Good</a>
                <a target='_blank' rel="noopener noreferrer" href='https://www.tiktok.com/business/vi?refer=tiktok_web' className={Styles.link}>Advertise</a>
                <a target='_blank' rel="noopener noreferrer" href='https://developers.tiktok.com/?refer=tiktok_web' className={Styles.link}>Developers</a>
                <a target='_blank' rel="noopener noreferrer" href='https://www.tiktok.com/transparency?lang=en' className={Styles.link}>Transparency</a>
            </div>
            <div className={Styles.item}>
                <a target='_blank' rel="noopener noreferrer"href='https://support.tiktok.com/en' className={Styles.link}>Help</a>
                <a target='_blank' rel="noopener noreferrer"href='https://www.tiktok.com/safety?lang=en' className={Styles.link}>Safety</a>
                <a target='_blank' rel="noopener noreferrer"href='https://www.tiktok.com/legal/terms-of-service?lang=en' className={Styles.link}>Terms</a>
                <a target='_blank' rel="noopener noreferrer"href='https://www.tiktok.com/legal/privacy-policy?lang=en' className={Styles.link}>Privacy</a>
                <a target='_blank' rel="noopener noreferrer"href='https://www.tiktok.com/creators/creator-portal/en-us/' className={Styles.link}>Creator Portal</a>
                <a target='_blank' rel="noopener noreferrer"href='https://www.tiktok.com/community-guidelines?lang=en' className={Styles.link}>Community Guidelines</a>
                <a target='_blank' rel="noopener noreferrer"href='https://www.tiktok.com/legal/copyright-policy?lang=en' className={Styles.link}>Copyright</a>
            </div>
            <div className={Styles.moreWord}>
                <span>More</span>
            </div>
            <span className={Styles.Copyright}>© 2021 TikTok</span>
        </div>
    )
}

export default Footer
