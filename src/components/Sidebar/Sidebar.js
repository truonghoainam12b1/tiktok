import { OverlayScrollbarsComponent } from 'overlayscrollbars-react'

import styles from './Sidebar.module.scss'

function Sidebar({
    children = null
}){
    return (   
        <OverlayScrollbarsComponent
            className={styles.wrapper}
            options={{
                scrollbars: {
                autoHide: 'leave',
                autoHideDelay: 0
                    }
            }}     
        >
            {children}
        </OverlayScrollbarsComponent>        
    )
}

export default Sidebar
