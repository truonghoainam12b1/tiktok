
import { Link } from 'react-router-dom'
import { Waypoint } from 'react-waypoint'
import React,{ useEffect , useRef } from 'react'
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle , faMusic , faHeart , faCommentDots , faShare } from '@fortawesome/free-solid-svg-icons'
import { BsPlayFill , BsFillVolumeUpFill , BsFillPauseFill , BsVolumeMuteFill } from 'react-icons/bs';
import { FiFlag } from 'react-icons/fi';

import Button from '~/packages/namth-button'
import styles from './Post.module.scss'
import Popper from '~/components/Popper'
import AccountPreview from '~/components/AccountPreview'
import SharePost from '~/components/SharePost'
// import UserContext from '~/contexts/UserContext'
import { selectors as UserSelectors } from '~/state/user'

function PostItem ({
    data ,
    type,
    isWaypoint = false,
    isMuted,
    isPlaying = false,
    stopWhenPause = false,
    onToggleMute = () => {},
    onWaypointEnter = () => {},
    onShowDetail = () => {},
    getVideoRef = () => {},
    onTogglePlay = () => {},
    onToggleLike = () => {},
    onToggleFollow = () => {},
    onClickComment = () => {},
    onClickShare = () => {},
    onClickCopy = () => {},
}) {
    const videoRef = useRef(null)
    const history = useHistory()
    const currentUser = useSelector(UserSelectors.getCurrentUser)

    useEffect(() => {
        if(!videoRef.current) return
        if(isPlaying) {
            videoRef.current.play()
        } else {
            videoRef.current.pause()
            if(stopWhenPause) videoRef.current.currentTime = 0
        }

    },[isPlaying , stopWhenPause])
    const handleShowProfile = (nickname) => {
        history.push(`/@${nickname}`)
    }
    return (
        <div className={styles.wrapper} >
            <img src={data?.user.avatar} alt={data?.user.nickname} className={styles.avatar}/>
            <div className={styles.itemBox}>
                <div className={styles.itemTitle}>
                    <Popper
                        placement= 'bottom'
                        interactive
                        duration={[300, 250]}
                        minWidth={350}
                        maxWidth={350}
                        appendTo={() => document.body}
                        delay={[600 , 100]}
                        offset={[45, 40]}
                        render={() => (
                            <AccountPreview
                                avatar={data?.user.avatar}
                                nickname={data?.user.nickname}
                                tick={data?.user.tick}
                                fullname={data?.fullname}
                                followers={data?.user.followers_count}
                                likes={data?.user.likes_count}
                                bio={data?.user.bio}
                                isFollow={false}    
                                showBio={true}
                                showProfile={handleShowProfile}
                            />
                        )}
                    >
                        <Link to={`/@${data?.user.nickname}`}className={styles.nickname}>{data?.user.nickname}</Link>
                    </Popper>
                    {data?.user.tick && <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/>}
                    <span className={styles.name}>{data?.full_name}</span>
                    <span className={styles.dot}> · </span>
                    <span className={styles.time}>{data?.published_at_from_now}</span>
                </div>
                <p className={styles.content}>{data?.description}<span>{data?.hashtag}</span></p>
                <div className={styles.audio}>
                    <FontAwesomeIcon className={styles.music} icon={faMusic}/>
                    <span className = {styles.song}>{data?.song}</span>
                </div>
                {currentUser?.id === data?.user_id || type === 'following' ||
                    <div className={styles.follow} onClick={() => onToggleFollow(data?.user)}>
                            {data.user.is_followed ? <Button type="borderBlack" size="s" >Following</Button> : 
                                <Button type="border" size="s" >Follow</Button>
                            }
                    </div>
                }
                
                <div className = {styles.postBox}>
                    <div className = {styles.videoBox}>
                        {isWaypoint && (
                            <Waypoint
                                topOffset = {data.computed_top_offset}
                                bottomOffset = {data.computed_bottom_offset}
                                onEnter = {() => onWaypointEnter(data)}
                            >
                                <div className = {styles.waypoint}></div>
                            </Waypoint>
                        )}
                        <video
                            ref={ref => {
                                videoRef.current = ref
                                getVideoRef(ref, data)
                            }}
                            loop
                            muted={isMuted}
                            style={{
                                width: data.computed_video_width
                            }} 
                            className={styles.video} 
                            poster={data.thumb_url}
                            onClick={() => onShowDetail(data)}>
                                <source src={data.file_url} type={data.video_mime_type}></source>
                        </video>
                        <div className={styles.report}>
                            <FiFlag className={styles.flapIcon}/>
                            <span>Report</span>
                        </div>
                        <div className={styles.play} onClick={() => onTogglePlay(data)}>
                            {isPlaying ?  <BsFillPauseFill/> : <BsPlayFill/>}
                        </div>
                        <div className={[styles.volume , isMuted ? styles.block : ''].join(' ')}
                             onClick={() => onToggleMute(data)}
                             >
                            {isMuted ? <BsVolumeMuteFill/> : <BsFillVolumeUpFill/>  }
                        </div>
                    </div>
                    <div className = {styles.reaction}>
                        <div className = {styles.reactionItem} >
                            <div className = {styles.reactionIcon} onClick={() => {onToggleLike(data)}}>
                                <FontAwesomeIcon className={[styles.icon , data.is_liked ? styles.liked : ''].join(' ')} icon={faHeart}/>
                            </div>
                            <p className = {styles.amount}>{data?.likes_count}</p>
                        </div>
                        <div className = {styles.reactionItem} onClick={() => onClickComment(data)}>
                            <div className = {styles.reactionIcon}>
                                <FontAwesomeIcon className={styles.icon} icon={faCommentDots}/>
                            </div>
                            <p className = {styles.amount}>{data?.comments_count}</p>
                        </div>
                        <Popper
                            placement= 'top'
                            interactive
                            duration={[300, 250]}
                            minWidth={350}
                            maxWidth={350}
                            delay={[600 , 100]}
                            appendTo={() => document.body}
                            offset={[130, 15]}
                            render={() => (
                                <SharePost 
                                    post={data}
                                    onClickShare={onClickShare}
                                    onClickCopy={onClickCopy}
                                    />
                            )}
                        >
                        <div className = {styles.reactionItem} >
                            <div className = {styles.reactionIcon}>
                                <FontAwesomeIcon className={styles.icon} icon={faShare}/>
                            </div>
                            <p className = {styles.amount}>{data?.shares_count}</p>
                        </div>
                    </Popper>
                        
                    </div>
                </div>
            </div>
        </div>
    ) 
}

export default React.memo(PostItem)

