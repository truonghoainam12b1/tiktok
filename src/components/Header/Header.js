import { Grid, Row , Column } from '@mycv/mycv-grid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch , faTimesCircle , faEllipsisV } from '@fortawesome/free-solid-svg-icons'
import { RiUploadCloudLine } from 'react-icons/ri'
import { FaTelegramPlane } from 'react-icons/fa'
import { BiMessageAltMinus } from 'react-icons/bi'
import { Link } from 'react-router-dom'
import { IoReloadCircleSharp } from 'react-icons/io5';
import { useSelector } from 'react-redux'


import Button from '~/packages/namth-button'
import { selectors as UserSelectors } from '~/state/user'
import logoDark from '~/assets/img/logo-dark.svg'
import logoTextDark from '~/assets/img/logo-text-dark.svg'
import config from '~/config'
import styles from './Header.module.scss'
import Tooltip from '~/components/Tooltip'
import Popper from '~/components/Popper'
import SearchPreview , {ResultItem} from '~/components/SearchPreview'
// import UserContext from '~/contexts/UserContext'
import OptionPreview from '~/components/OptionPreview'



function Header({
    isSearching = false,
    searchValue = '',
    searchResults = [] ,
    visibleSearch,
    onUpload = () => {},
    onLogin = () => {},
    onSearchClear = () => {},
    onSearchChange = () => {},
    onClickResultItem = () => {},
    onViewAllSearchResult = () => {},
    onClickLogOut = () => {},
    onMessage = () => {},
    onClickViewProfile = () => {},
    onFocusSearch = () => {},
    onClickOutside = () => {},
}) {
    const currentUser = useSelector(UserSelectors.getCurrentUser)
    const isAuthenticated = useSelector(UserSelectors.isAuthenticated)
    const renderSearchPreview = () => {
        return (
            <SearchPreview
                searchValue={searchValue}
                onViewAll={onViewAllSearchResult}
            >
                {searchResults.map(item => 
                    <ResultItem
                        key= {item.id}
                        title = {item.nickname}
                        description = {item.full_name}
                        tick = {item.tick}
                        linkTo = {`/@${item.nickname}`}
                        onClick = {onClickResultItem}
                    />
                )}
            </SearchPreview>
        )
    }
    const renderOptionPreview = (currentUser) => {
        return (
            <OptionPreview 
                onClickLogOut={onClickLogOut}
                onClickViewProfile={() => onClickViewProfile(currentUser)}
            />
        )
    }
    return (
        <header className={styles.wrapper}>
            <Grid type = "wide" maxWidth={1100}>
                <Row>
                    <Column size={12} sizeTablet={12} sizeDesktop={12}>

                        <div className={styles.content}>
                            <Link to={config.routes.home} className={styles.logoBox}>
                                <img src={logoDark} alt="" />
                                <img src={logoTextDark} alt="Tiktok" />
                            </Link>
                            <Popper
                                visible={searchResults.length > 0 && visibleSearch === true}
                                interactive
                                onClickOutside={onClickOutside}
                                minWidth={400}
                                wrapperClassName={styles.previewWrapper}
                                render={renderSearchPreview}
                            >
                                <div className={styles.searchBox}>
                                    <input placeholder="Search accounts"
                                           onChange={onSearchChange}
                                           value={searchValue}
                                           onFocus={onFocusSearch}
                                    />
                                    <div className={styles.iconClear}>
                                        {!!searchValue && (
                                            <button 
                                               
                                                onClick={isSearching ? () => {} : onSearchClear}
                                                onMouseDown={e => e.preventDefault()}
                                            >
                                                {!isSearching ? <FontAwesomeIcon  icon={faTimesCircle} className={styles.timesCircleIcon}/> 
                                                                : <IoReloadCircleSharp className={styles.spinner}/>}
                                            </button>
                                        )}
                                    </div>
                                    <button className={styles.search}>
                                        <FontAwesomeIcon className={styles.searchIcon}icon={faSearch} onClick={onViewAllSearchResult}/>
                                    </button>
                                </div>
                            </Popper>
                            {
                                isAuthenticated ? (
                                    <div className={styles.headerRight}>
                                        <Tooltip content="Upload Video" interactive >
                                            <div className={styles.icon} onClick={onUpload} >
                                                <RiUploadCloudLine/>
                                            </div>
                                        </Tooltip>
                                        <Tooltip content="Message" interactive>
                                            <div className={styles.icon} onClick={onMessage}>
                                                <FaTelegramPlane/>                                
                                            </div>
                                        </Tooltip>
                                        <Tooltip content="Inbox">
                                            <div className={styles.icon}>
                                                <BiMessageAltMinus/>
                                            </div>
                                        </Tooltip>
                                        <Popper
                                            delay={[400,200]}
                                            offset={[-85, 10]}
                                            interactive
                                            minWidth={240}
                                            render={() => renderOptionPreview(currentUser.nickname)}
                                        >

                                            <div className={styles.avatarUser}>
                                                <img src={currentUser.avatar} alt={currentUser.title}/>
                                            </div>
                                        </Popper>
                                    </div> 
                                ) : (
                                    <div>
                                        <Button type="normal" size="m" underline onClick={onUpload}>Upload</Button>
                                        <Button type="primary" size="m" onClick={onLogin}>Log in</Button>
                                        <span className={styles.optionBox}>
                                            <FontAwesomeIcon className={styles.ellipsisIcon} icon={faEllipsisV}/>
                                        </span>
                                    </div>
                                )
                            }
                        </div>
                    </Column>
                </Row>
            </Grid>
        </header>
    )
}

export default Header
