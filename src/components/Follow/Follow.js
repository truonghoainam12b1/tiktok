
import styles from './Follow.module.scss'

import { Grid , Row } from '@mycv/mycv-grid'
import FollowItem from './FollowItem'

function Follow({
    data,
    isPlaying,
    onLoadMore = () => {},
    onMouseEnter = () => {},
    onClick = () => {},
    onClickFollow = () => {},
}) {
    return(
        <div className={styles.wrapper}>
            <Grid>
                <Row>
                    {data.map((account , index) => <FollowItem
                        key={account.id}
                        data={account}
                        isLast={data.length - 1 === index}
                        onEnter={onLoadMore}
                        onMouseEnter={onMouseEnter}
                        isPlaying={isPlaying(account)}
                        onClick={onClick}
                        onClickFollow={onClickFollow}
                    />)}
                </Row>        
            </Grid>
        </div>
    )
}

export default Follow