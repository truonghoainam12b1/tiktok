import { Link } from 'react-router-dom'
import { Column} from '@mycv/mycv-grid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { Waypoint } from 'react-waypoint'
import { Fragment , useRef ,useEffect } from 'react'

import styles from './Follow.module.scss'
import Button from '~/packages/namth-button'


function FollowItem ({
    data,
    isLast = false,
    isPlaying = false,
    onEnter = () => {},
    onLeave = () => {},
    onMouseEnter = () => {},
    onClick = () => {},
    onClickFollow = () => {},
}) {
    const videoRef = useRef(null)
    const props = {}
    let Component = Fragment

    useEffect(() => {
        if(!videoRef) return
        if(isPlaying) {
            videoRef.current.play()
        } else {
            videoRef.current.pause()
            videoRef.current.currentTime = 0
        }
    },[isPlaying])
    
    if(isLast) {
        Component = Waypoint
        props.onEnter = onEnter
        props.onLeave = onLeave
    }
    return(
        <Column size={12} sizeTablet={6} sizeDesktop={4}>
            <Component {...props}>
                <Link  
                    to={`/@${data.nickname}`}
                    className={styles.itemBox}
                    target='_blank'
                    onMouseEnter={() => onMouseEnter(data)}
                    onClick={() => onClick(data)}
                >
                    <video
                        className={styles.itemVideo} 
                        ref={videoRef}
                        muted
                        poster={data.popular_post.thumb_url}
                    >
                        <source src={data.popular_post?.file_url}></source>
                    </video>
                    <div className={styles.itemInfo}>
                        <img src={data.avatar} alt={data.nickname} className={styles.avatar}></img>
                        <p className={styles.name}>{data.full_name}</p>
                        <div className={styles.nickname}>
                            <span>{data.nickname}</span>
                            {data.tick && <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/>}
                        </div>
                        <Button type='primary' className={styles.btnFollow} size='ss' onClick={onClickFollow}>Follow</Button>
                    </div>
                </Link>
            </Component>
        </Column>
    )
}

export default FollowItem