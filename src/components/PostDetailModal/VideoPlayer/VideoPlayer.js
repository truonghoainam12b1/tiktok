import { useEffect , useRef , useState} from 'react'

import styles from './VideoPlayer.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faVolumeUp , faChevronLeft , faChevronRight , faPlay , faVolumeMute } from '@fortawesome/free-solid-svg-icons'
import { FiFlag } from 'react-icons/fi';


function VideoPlayer ({ 
    postDetail ,
    onNext = true,
    onPrev = true,
    isMuted ,
    isPlaying = false,
    getVideoRef = () => {},
    onPrevVideo = () => {},
    onNextVideo = () => {},
    onTogglePlaying = () => {},
    onToggleMute = () => {},
    onClickPlay = () => {},
}) {
    const videoRef = useRef(null)
    // const [ video , setVideo ] = useState(null)
    useEffect(() => {
        console.log(isPlaying)
        if(!videoRef.current) return
        if(isPlaying) {
            // setVideo(videoRef.current)
            videoRef.current.play()
        } else {
            // setVideo(videoRef.current)
            videoRef.current.pause()
        }

    } , [isPlaying])
    return(
        <div className={styles.wrapper} >
            <img className={styles.thumbnail} src={postDetail.thumb_url} alt=''/>
            <div className={styles.overlay}></div>
            <video
                src={postDetail.file_url} 
                className={styles.video} 
                loop
                muted={isMuted}
                ref = {(ref) => {
                    videoRef.current = ref
                    getVideoRef(ref)}}
                onClick={onTogglePlaying}
            ></video>
            <div className={styles.volume}
                 onClick={() => onToggleMute(postDetail)} 
            >
                {isMuted ? <FontAwesomeIcon className={styles.volumeIcon} icon={faVolumeMute}/>
                            : <FontAwesomeIcon className={styles.volumeIcon} icon={faVolumeUp}/>
                }
            </div>
            <div className={styles.report}>
                <FiFlag className={styles.flapIcon}/>
                <span>Report</span>
            </div>
            {onPrev && (
                <div className={styles.controlLeft} onClick={onPrevVideo}>
                    <FontAwesomeIcon className={styles.controlIcon} icon={faChevronLeft}/>
                </div>
            )}
            {onNext && (
                <div className={styles.controlRight} onClick={onNextVideo}>
                    <FontAwesomeIcon className={styles.controlIcon} icon={faChevronRight}/>
                </div>
            )}
            
            {isPlaying || <FontAwesomeIcon className={styles.controlPlay} icon={faPlay} onClick={onClickPlay}/>}
        </div>
    )
}

export default VideoPlayer
