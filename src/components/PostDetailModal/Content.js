
import styles from './PostDetailModal.module.scss'

function Content ({ children = null}) {
    return(
        <div className={styles.postContent}>
            {children}
        </div>
    )
}

export default Content
