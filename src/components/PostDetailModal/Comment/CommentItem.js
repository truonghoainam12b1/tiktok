import { AiOutlineHeart } from 'react-icons/ai'
import { BiDotsHorizontalRounded } from 'react-icons/bi'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { Waypoint } from 'react-waypoint'
import { Fragment } from 'react'

import Popper from '~/components/Popper'
import AccountPreview from '~/components/AccountPreview'
import styles from './Comment.module.scss'

function ComponentItem ({
   data = [],
   postAuthorId,
   isLast = false,
   onEnter = () => {},
   onLeave = () => {},
   showProfile = () => {},
}) {
   let Component =  Fragment
   const props = {}
   if(isLast) {
      Component = Waypoint
      props.onEnter = onEnter
      props.onLeave = onLeave
   }
   return (
      <Component {...props}>
            <div className={styles.item}>
               <div className={styles.avatar}>
                  <img src={data?.user.avatar} alt=''/>
               </div>
               <div className={styles.info}>
               <Popper
                     placement= 'bottom'
                     interactive
                     duration={[300, 250]}
                     minWidth={350}
                     maxWidth={350}
                     delay={[400 , 100]}
                     offset={[-120, 20]}
                     appendTo={() => document.body}
                     render={() => (
                        <AccountPreview
                              avatar={data?.user.avatar}
                              nickname={data?.user.nickname}
                              tick={data?.user.tick}
                              fullname={data.full_name}
                              followers={data?.user.followers_count}
                              likes={data?.user.likes_count}
                              isFollow={false}    
                              bio={data?.user.bio}
                              showBio={true}
                              showProfile={showProfile}
                        />
                     )}
                  >
                     <div className={styles.infoName}>
                                 <h3>{data?.full_name}</h3>
                                 {data?.user.tick ? <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/> : ''}
                                 {postAuthorId === data?.user.id ? <span className={styles.author}> · Creator</span> : ''}
                              </div>
                  </Popper>
                  <p>{data.comment}</p>
                  <span>{data?.created_at_from_now}</span>
               </div>
               <div className={styles.icon}>
                  <AiOutlineHeart/>
                  <span>{data?.likes_count}</span>
               </div>
               <div className={styles.option}>
                     <BiDotsHorizontalRounded/>
               </div>
            </div>
         </Component>
      
         
   )
}

export default ComponentItem