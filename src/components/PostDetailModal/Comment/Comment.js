
import { FiAtSign } from 'react-icons/fi'
import { BiHappy } from 'react-icons/bi'

import { OverlayScrollbarsComponent } from 'overlayscrollbars-react'
import { useSelector } from 'react-redux'

import { selectors as UserSelectors } from '~/state/user'
import styles from './Comment.module.scss'
import Button from '~/packages/namth-button'
import Picker, { SKIN_TONE_MEDIUM_DARK } from "emoji-picker-react";
// import UserContext from '~/contexts/UserContext'

function Comment({
    children = null,
    isChosenEmoji = false,
    onEmojiClick = () => {},
    onToggleEmojiIcon = () => {},
    onClickPost = () => {},
    comment, 
    onChangeText = () => {},
    onClickLogin = () => {},
}) {
    const isAuthenticated = useSelector(UserSelectors.isAuthenticated)
    console.log(comment.emoji)
    return (
        <div className={styles.wrapper}>
                {isAuthenticated ? (
                    <div className={styles.comment}> 
                        <OverlayScrollbarsComponent 
                            className={styles.inner}
                            options={{
                                scrollbars: {
                                autoHide: 'leave',
                                autoHideDelay: 0
                                    }
                            }}        
                        >
                            {children}        
                        </OverlayScrollbarsComponent>
                        
                        <div className={styles.comment_post}>
                            <div className={styles.comment_input}>
                                <input type="text" 
                                    placeholder="Add comment..." 
                                    // value={comment} 
                                    onChange={onChangeText}
                                />
                                <div className={styles.comment_icon}>
                                    {/* <div>
                                        <FiAtSign/>
                                    </div> */}
                                    <div onClick={onToggleEmojiIcon}>
                                        <BiHappy/>
                                    </div>
                                </div>
                            </div>
                            <div className={[styles.comment_btn , !!comment ? styles.comment_btn_on : '' ].join(' ')} onClick={onClickPost}>
                                Post
                            </div>
                        </div>
                        {isChosenEmoji && <Picker 
                            disableAutoFocus={true}
                            skinTone={SKIN_TONE_MEDIUM_DARK}
                            groupNames={{ smileys_people: "PEOPLE" }}
                            native
                            pickerStyle={{ position: 'fixed' , right: '70px' , bottom: '70px' }}
                            onEmojiClick={onEmojiClick}
                        >
                        </Picker>}
                    </div>
                ) : (
                    <div className = {styles.box}>
                        <p className={styles.title}>
                            Login to see comments
                        </p>
                        <p className={styles.description}>
                            Login to see comments and like the video.
                        </p>
                        <Button type='primary' size='l' onClick={onClickLogin}>Log in</Button>
                        <p className={styles.question}>Don’t have an account?<span>Sign up</span></p>
                    </div>
                )}
            </div>
    )
}

export default Comment