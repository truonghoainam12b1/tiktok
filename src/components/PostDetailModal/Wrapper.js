
import styles from './PostDetailModal.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

import LogoDetail from '~/assets/img/logoDetail.png'

function Wrapper ({
    children = null ,
    onRequestClose = () => {},
}) {
    return (
        <div className={styles.wrapper}>
            <button className={styles.closeBtn} onClick={onRequestClose}>
                <FontAwesomeIcon className={styles.timesIcon} icon={faTimes}/>
            </button>
            <img className={styles.logoDetail} src={LogoDetail} alt=''></img>
            
            <div className={styles.inner}>
                {children}
            </div>
        </div>
    )
}

export default Wrapper

