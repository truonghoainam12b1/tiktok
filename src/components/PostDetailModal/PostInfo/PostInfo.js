
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle , faMusic , faHeart , faCommentDots } from '@fortawesome/free-solid-svg-icons'
import { FaWhatsappSquare , FaFacebook} from 'react-icons/fa';
import { AiFillTwitterCircle } from 'react-icons/ai';
// import { RiCodeBoxFill } from 'react-icons/ri';
import { IoIosLink } from 'react-icons/io';
// import config from '~/config'
import Toast from '~/components/Toast'
import Button from '../../../packages/namth-button'
import Tooltip from '../../Tooltip'
import Popper from '../../Popper'
import AccountPreview from '../../AccountPreview'

import styles from './PostInfo.module.scss'

function PostInfo ({
    postDetail=[],
    onToggleLike = () => {},
    onToggleFollow = () => {},
    onClickCopy = () => {},
    onClickShare = () => {},
    showProfile = () => {},
}) {
    const path = window.location.host

    return (
        <div className={styles.wrapper}>
            <div className={styles.itemBox}>
                <img src={postDetail.user.avatar} className={styles.itemAvatar} alt={postDetail.user.nickname}/>
                <div className={styles.itemName}>
                <Popper
                    placement= 'bottom'
                    interactive
                    duration={[300, 250]}
                    minWidth={350}
                    maxWidth={350}
                    delay={[600 , 100]}
                    offset={[35, 20]}
                    appendTo={() => document.body}
                    render={() => (
                        <AccountPreview
                            avatar={postDetail?.user.avatar}
                            nickname={postDetail?.user.nickname}
                            tick={postDetail?.user.tick}
                            fullname={postDetail.full_name}
                            followers={postDetail?.user.followers_count}
                            likes={postDetail?.user.likes_count}
                            isFollow={false}    
                            bio={postDetail?.user.bio}
                            showBio={true}
                            showProfile={showProfile}
                        />
                    )}
                >
                    <Link
                        to={`/@${postDetail.user.nickname}`}
                        className={styles.nickname}
                        >{postDetail.user.nickname}
                    </Link>
                    {postDetail?.user.tick && <FontAwesomeIcon className={styles.checkCircleIcon} icon={faCheckCircle}/>}
                </Popper>
                <p className={styles.name}>{postDetail?.full_name}
                    <span className={styles.dot}> · </span>
                    <span>{postDetail?.published_at_from_now}</span>
                    </p>
                </div>
                <div className={styles.follow} onClick={() => onToggleFollow(postDetail?.user)}>
                    {postDetail.user.is_followed ? <Button type="borderBlack" size="s" >Following</Button> : 
                        <Button type="border" size="s" >Follow</Button>
                    }
                </div>
            </div>
            <p className={styles.content}>{postDetail.description}<span>{postDetail?.hashtag}</span></p>
            <div className={styles.audio}>
                <FontAwesomeIcon className={styles.music} icon={faMusic}/>
                <span className = {styles.song}>{postDetail?.audioUrl}</span>
            </div>
            <div className={styles.reaction}>
                <div className={styles.reactionWrapper}>
                    <div className = {styles.reactionItem} >
                        <div className = {styles.reactionIcon} onClick={() => onToggleLike(postDetail)}>
                            <FontAwesomeIcon className={[styles.icon , postDetail.is_liked ? styles.liked : ''].join(' ')}
                             icon={faHeart}/>
                        </div>
                        <p className = {styles.amount}>{postDetail.likes_count}</p>
                    </div>
                    <div className = {styles.reactionItem} >
                        <div className = {styles.reactionIcon}>
                            <FontAwesomeIcon className={styles.icon} icon={faCommentDots}/>
                        </div>
                        <p className = {styles.amount}>{postDetail.comments_count}</p>
                    </div>
                </div>
                <div className = {styles.reactionBox} >
                    <p className = {styles.reactionShare}>Share to</p>
                    <Tooltip content="Share to Whatsapp" interactive>
                        <div className={styles.searchIcon} onClick={() => onClickShare('whatsapp',postDetail)}>
                            <FaWhatsappSquare className = {styles.whatsapp}/>
                        </div>
                    </Tooltip>
                    <Tooltip content="Share to Facebook" interactive>
                        <div className={styles.searchIcon} onClick={() => onClickShare('facebook',postDetail)}>
                            <FaFacebook className = {styles.facebook}/>
                        </div>
                    </Tooltip>
                    <Tooltip content="Share to Twitter" interactive>
                        <div className={styles.searchIcon} onClick={() => onClickShare('twitter',postDetail)}>
                            <AiFillTwitterCircle className = {styles.twitter}/>
                        </div>
                    </Tooltip>
                    {/* <Tooltip content="Embed" interactive>
                        <div className={styles.searchIcon}>
                            <RiCodeBoxFill className = {styles.code}/>        
                        </div>
                    </Tooltip> */}
                </div>
            </div>
            <div className = {styles.footer}>
                <div className = {styles.copyLink}>
                    <div className = {styles.link}>
                        {`${path}/${postDetail.user.nickname}/video/${postDetail.uuid}`}
                    </div>
                    <Toast message='Copied'>
                        <div className = {styles.copy}
                            onClick={() => onClickCopy(`${path}/${postDetail.user.nickname}/video/${postDetail.uuid}`)}>
                            <IoIosLink className={styles.copyIcon}/>
                            <span>COPY LINK</span>
                        </div>
                    </Toast>
                </div>
            </div>
        </div>
    )
 }

 export default PostInfo;